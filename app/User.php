<?php

namespace App;

use App\Models\Role;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, Sortable;

    public function __construct()
    {
        $this->dt = date('Y-m-d H:i:s');
    }

    /**
     * sortable
     * https://github.com/Kyslik/column-sortable
     */
    public $sortable = ['name', 'email', 'company'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'company',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Accessor
     * https://laravel.com/docs/5.8/eloquent-mutators#defining-an-accessor
     */
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function getEmailAttribute($value)
    {
        return strtolower($value);
    }

    /**
     * Scope
     * https://laravel.com/docs/5.8/eloquent#local-scopes
     */
    public function scopeAdmin($query)
    {
        $role = Role::admin()->first();
        return $query->where('role_id', $role->id);
    }

    public function scopeClient($query)
    {
        $role = Role::client()->first();
        return $query->where('role_id', $role->id);
    }

    public function scopeSearchWithRole($query, $keywords)
    {
        return $query->when($keywords, function ($user) use ($keywords) {
            $user->where(function ($q) use ($keywords) {
                $q->where('name', 'LIKE', '%' . $keywords . '%')
                    ->orWhere('email', 'LIKE', '%' . $keywords . '%')
                    ->orWhereHas('role', function ($role) use ($keywords) {
                        $role->where('name', 'LIKE', '%' . $keywords . '%');
                    });
            });
        });
    }

    public function scopeActive($query)
    {
        return $query->whereNull('deleted_at');
    }

    /**
     * Relation
     * https://laravel.com/docs/5.8/eloquent-relationships
     */
    public function role()
    {
        return $this->hasOne('App\Models\Role', 'id', 'role_id');
    }

    public function packages()
    {
        return $this->belongsToMany('App\Models\Package', 'user_packages', 'user_id')
            ->withPivot(['created_at', 'updated_at', 'expired_at'])
            ->wherePivot('expired_at', '>=', $this->dt);
    }

    /**
     * Query
     */
    public function getListClient($keywords = null)
    {
        return $this->client()
            ->with('packages')
            ->when($keywords, function ($user) use ($keywords) {
                $user->where(function ($query) use ($keywords) {
                    $query->where('name', 'LIKE', '%' . $keywords . '%')
                        ->orWhere('email', 'LIKE', '%' . $keywords . '%')
                        ->orWhere('company', 'LIKE', '%' . $keywords . '%')
                        ->orWhereHas('packages', function ($package) use ($keywords) {
                            $package->where('name', 'LIKE', '%' . $keywords . '%');
                        });
                });
            });
    }
}
