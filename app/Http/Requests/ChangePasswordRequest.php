<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    public function __construct()
    {
        $this->min_char_password = 6;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'nullable|confirmed|min:' . $this->min_char_password,
        ];
    }

    public function messages()
    {
        return [
            'password.min' => __('errors.not_valid', ['field' => __('auth.password')]),
            'password.confirmed' => __('errors.not_valid', ['field' => __('auth.confirm_password')]),
        ];
    }
}
