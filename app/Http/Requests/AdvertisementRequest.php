<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdvertisementRequest extends FormRequest
{
    public function __construct()
    {
        $this->max_char = 255;
        $this->max_video_size = 15000; // in kb
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:' . $this->max_char,
            'advertiser' => 'nullable|string|max:' . $this->max_char,
            'type_of_advertiser_company' => 'nullable|string|max:' . $this->max_char,
            'video' => 'nullable|file|mimes:mp4|max:' . $this->max_video_size,
        ];
    }

    public function messages()
    {
        return [
            'name.string' => __('errors.not_valid', ['field' => __('wording.ads.name')]),
            'name.max' => __('errors.not_valid', ['field' => __('wording.ads.name')]),
            'advertiser.string' => __('errors.not_valid', ['field' => __('wording.ads.advertiser')]),
            'advertiser.max' => __('errors.not_valid', ['field' => __('wording.ads.dvertiser')]),
            'type_of_advertiser_company.string' => __('errors.not_valid', ['field' => __('wording.ads.company_type')]),
            'type_of_advertiser_company.max' => __('errors.not_valid', ['field' => __('wording.ads.company_type')]),
            'video.file' => __('errors.not_valid', ['field' => __('wording.ads.video')]),
            'video.mimes' => __('errors.not_valid', ['field' => __('wording.ads.video')]),
            'video.max' => __('errors.max', ['field' => __('wording.ads.video'), 'max' => ($this->max_video_size / 1000) . ' MB']),
        ];
    }
}
