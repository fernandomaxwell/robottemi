<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    public function __construct()
    {
        $this->max_char = 255;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'keywords' => 'nullable|string|max:' . $this->max_char,
        ];
    }

    public function messages()
    {
        return [
            'keywords.string' => __('errors.not_valid', ['field' => __('wording.keywords')]),
            'keywords.max' => __('errors.not_valid', ['field' => __('wording.keywords')]),
        ];
    }
}
