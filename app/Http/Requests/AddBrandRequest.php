<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddBrandRequest extends FormRequest
{
    public function __construct()
    {
        $this->max_char = 255;
        $this->max_char_username = 200;
        $this->min_char_password = 6;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:' . $this->max_char,
            'username' => 'required|string|max:' . $this->max_char_username,
            'category' => 'nullable|string|max:' . $this->max_char,
            'password' => 'required|confirmed|min:' . $this->min_char_password,
        ];
    }

    public function messages()
    {
        return [
            'name.string' => __('errors.not_valid', ['field' => __('wording.brand.name')]),
            'name.max' => __('errors.not_valid', ['field' => __('wording.brand.name')]),
            'username.required' => __('errors.required', ['field' => __('auth.username')]),
            'username.string' => __('errors.not_valid', ['field' => __('auth.username')]),
            'username.max' => __('errors.not_valid', ['field' => __('auth.username')]),
            'category.string' => __('errors.not_valid', ['field' => __('wording.brand.category')]),
            'category.max' => __('errors.not_valid', ['field' => __('wording.brand.category')]),
            'password.required' => __('errors.required', ['field' => __('auth.password')]),
            'password.min' => __('errors.not_valid', ['field' => __('auth.password')]),
            'password.confirmed' => __('errors.not_valid', ['field' => __('auth.confirm_password')]),
        ];
    }
}
