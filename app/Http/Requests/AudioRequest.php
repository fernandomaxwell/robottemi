<?php

namespace App\Http\Requests;

use App\Models\AudioType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AudioRequest extends FormRequest
{
    public function __construct()
    {
        $this->max_char = 255;
        $this->max_audio_size = 5000; // in kb
        $this->allowed_type = AudioType::get()->pluck('id');
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:' . $this->max_char,
            'audio' => 'nullable|file|mimes:mp3|max:' . $this->max_audio_size,
            'type' => 'required|integer|' . Rule::in($this->allowed_type),
        ];
    }

    public function messages()
    {
        return [
            'name.string' => __('errors.not_valid', ['field' => __('wording.name')]),
            'name.max' => __('errors.not_valid', ['field' => __('wording.name')]),
            'audio.file' => __('errors.not_valid', ['field' => __('wording.audio')]),
            'audio.mimes' => __('errors.not_valid', ['field' => __('wording.audio')]),
            'audio.max' => __('errors.max', ['field' => __('wording.audio'), 'max' => ($this->max_audio_size / 1000) . ' MB']),
            'type.required' => __('errors.required', ['field' => __('wording.type')]),
            'type.integer' => __('errors.not_valid', ['field' => __('wording.type')]),
            'type.in' => __('errors.not_valid', ['field' => __('wording.type')]),
        ];
    }
}
