<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\LeadGeneration;
use Illuminate\Http\Request;

class LeadGenerationController extends Controller
{
    public function __construct()
    {
        $this->lead_generation_db = new LeadGeneration();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $title = __('menu.lead_generation');

        // Get data
        $lead_generations = $this->lead_generation_db->getList($request->keywords)->sortable()->paginate();

        // Append filter as query string
        $lead_generations->appends($request->except('page'));

        return view('lead-generation.browse', compact(
            'request',
            'title',
            'lead_generations'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
