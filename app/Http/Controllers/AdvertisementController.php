<?php

namespace App\Http\Controllers;

use App\Helpers\Audio as AudioHelper;
use App\Helpers\Advertisement as AdvertisementHelper;
use App\Http\Requests\AdvertisementRequest;
use App\Http\Requests\SearchRequest;
use App\Models\Advertisement;
use App\Models\AudioType;
use DB;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    public function __construct()
    {
        $this->advertisement_db = new Advertisement();

        $this->advertisement_helper = new AdvertisementHelper();
        $this->audio_helper = new AudioHelper();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $title = __('menu.advertisement');

        $brand = request()->get('brand');
        $audio_types = AudioType::get();

        // Check type
        $request->type = $request->type ? $request->type : $audio_types->first()->name;
        $this->audio_helper->validateType($request->type);

        $advertisements = $this->advertisement_db->getList($request->brand, $request->type, $request->keywords)->paginate();

        // Append filter as query string
        $advertisements->appends($request->except('page'));

        return view('advertisement.browse', compact(
            'request',
            'title',
            'brand',
            'audio_types',
            'advertisements'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = __('wording.add') . ' ' . __('menu.advertisement');

        $brand = request()->get('brand');
        $audio_types = AudioType::get();
        $total_priority = $this->advertisement_helper->setTotalPriority($brand);

        return view('advertisement.add', compact(
            'title',
            'brand',
            'audio_types',
            'total_priority'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertisementRequest $request)
    {
        DB::beginTransaction();
        try {
            $audio_type = AudioType::findOrFail($request->type);

            if ($request->priority) {
                $this->advertisement_db->setNullPriority($request->priority, $request->brand);
            }

            $advertisement = $this->advertisement_db->create([
                'name' => $request->name,
                'advertiser' => $request->advertiser,
                'path' => $this->advertisement_helper->uploadVideo($request->file('video')),
                'type_of_advertiser_company' => $request->type_of_advertiser_company,
                'priority' => $request->priority,
                'type_id' => $audio_type->id,
                'type' => $audio_type->name,
                'user_id' => $request->brand,
            ]);

            DB::commit();
        } catch (\Exception $e) {
            // dump($e); die;
            DB::rollBack();
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.add') . ' ' . ucwords(__('menu.advertisement')));
        }

        return redirect('advertisement?brand=' . $request->brand)->with('success', __('wording.success') . ' ' . __('wording.add') . ' ' . ucwords(__('menu.advertisement')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = __('wording.edit') . ' ' . __('menu.advertisement');

        $brand = request()->get('brand');
        $audio_types = AudioType::get();
        $advertisement = $this->advertisement_db->brand($brand->id)->active()->findOrFail($id);
        $total_priority = $this->advertisement_helper->setTotalPriority($brand);

        return view('advertisement.edit', compact(
            'title',
            'brand',
            'audio_types',
            'advertisement',
            'total_priority'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdvertisementRequest $request, $id)
    {
        // Check if id exist
        $advertisement = $this->advertisement_db->brand($request->brand)->active()->findOrFail($id);

        DB::beginTransaction();
        try {
            if ($request->priority) {
                $this->advertisement_db->setNullPriority($request->priority, $request->brand);
            }

            $audio_type = AudioType::findOrFail($request->type);

            // Assign data
            $advertisement_data = [
                'name' => $request->name,
                'advertiser' => $request->advertiser,
                'type_of_advertiser_company' => $request->type_of_advertiser_company,
                'priority' => $request->priority,
                'type_id' => $audio_type->id,
                'type' => $audio_type->name,
            ];

            // Check if image is given
            $deleted_video_url = null;
            if ($path = $this->advertisement_helper->uploadVideo($request->file('video'))) {
                $advertisement_data['path'] = $path;
                $deleted_video_url = $advertisement->path;
            }

            $this->advertisement_db->find($id)->update($advertisement_data);

            if ($deleted_video_url) {
                $this->advertisement_helper->deleteVideo($deleted_video_url);
            }

            DB::commit();
        } catch (\Exception $e) {
            // dump($e);die;
            DB::rollBack();
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.edit') . ' ' . ucwords(__('menu.advertisement')));
        }

        return back()->with('success', __('wording.success') . ' ' . __('wording.edit') . ' ' . ucwords(__('menu.advertisement')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Check if id exist
        $advertisement = $this->advertisement_db->brand($request->brand)->active()->findOrFail($id);

        try {
            $advertisement->status = 0;
            $advertisement->save();
        } catch (\Exception $e) {
            // dump($e); die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.delete') . ' ' . ucwords(__('menu.advertisement')));
        }

        return back()->with('success', __('wording.success') . ' ' . __('wording.delete') . ' ' . ucwords(__('menu.advertisement')));
    }
}
