<?php

namespace App\Http\Controllers;

class SettingController extends Controller
{
    public function changeLanguage($code)
    {
        session(['language' => $code]);
        return back();
    }
}
