<?php

namespace App\Http\Controllers;

use App\Helpers\Audio as AudioHelper;
use App\Http\Requests\AudioRequest;
use App\Http\Requests\SearchRequest;
use App\Models\Audio;
use App\Models\AudioType;
use Illuminate\Http\Request;

class AudioController extends Controller
{
    public function __construct()
    {
        $this->audio_db = new Audio();

        $this->audio_helper = new AudioHelper();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $title = __('menu.audio');

        $brand = request()->get('brand');
        $audio_types = AudioType::get();

        // Check type
        $request->type = $request->type ? $request->type : $audio_types->first()->name;
        $this->audio_helper->validateType($request->type);

        $audios = $this->audio_db->getList($request->brand, $request->type, $request->keywords)->paginate();

        // Append filter as query string
        $audios->appends($request->except('page'));

        return view('audio.browse', compact(
            'request',
            'title',
            'brand',
            'audio_types',
            'audios'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = __('wording.add') . ' ' . __('menu.audio');

        $brand = request()->get('brand');
        $audio_types = AudioType::get();

        return view('audio.add', compact(
            'title',
            'brand',
            'audio_types'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AudioRequest $request)
    {
        try {
            $audio_type = AudioType::findOrFail($request->type);
            $last_audio_priority = $this->audio_db->getLatestPriority($request->brand)->first();

            $audio = $this->audio_db->create([
                'name' => $request->name,
                'path' => $this->audio_helper->uploadAudio($request->file('audio')),
                'type_id' => $audio_type->id,
                'type' => $audio_type->name,
                'status' => 1, // default
                'priority' => $last_audio_priority ? ($last_audio_priority->priority + 1) : 1,
                'user_id' => $request->brand,
            ]);
        } catch (\Exception $e) {
            // dump($e);die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.add') . ' ' . ucwords(__('menu.audio')));
        }

        return redirect('audio?brand=' . $request->brand)->with('success', __('wording.success') . ' ' . __('wording.add') . ' ' . ucwords(__('menu.audio')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = __('wording.edit') . ' ' . __('menu.audio');

        $brand = request()->get('brand');
        $audio_types = AudioType::get();
        $audio = $this->audio_db->brand($brand->id)->active()->findOrFail($id);

        // Set type id from type name if not exist
        if (!$audio->type_id) {
            $audio_type = AudioType::where('name', $audio->type)->first();
            if ($audio_type) {
                $audio->type_id = $audio_type->id;
            }
        }

        return view('audio.edit', compact(
            'title',
            'brand',
            'audio_types',
            'audio'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AudioRequest $request, $id)
    {
        // Check if id exist
        $audio = $this->audio_db->brand($request->brand)->active()->findOrFail($id);

        try {
            $audio_type = AudioType::findOrFail($request->type);
            $audio_data = [
                'name' => $request->name,
                'type_id' => $audio_type->id,
                'type' => $audio_type->name,
            ];

            // Check if image is given
            $deleted_audio_url = null;
            if ($path = $this->audio_helper->uploadAudio($request->file('audio'))) {
                $audio_data['path'] = $path;
                $deleted_audio_url = $audio->path;
            } else {
                if (!$audio->path) {
                    return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.edit') . ' ' . ucwords(__('menu.audio')));
                }
            }

            $audio->update($audio_data);

            if ($deleted_audio_url) {
                $this->audio_helper->deleteAudio($deleted_audio_url);
            }
        } catch (\Exception $e) {
            // dump($e); die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.edit') . ' ' . ucwords(__('menu.audio')));
        }

        return back()->with('success', __('wording.success') . ' ' . __('wording.edit') . ' ' . ucwords(__('menu.audio')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Check if id exist
        $audio = $this->audio_db->brand($request->brand)->active()->findOrFail($id);

        try {
            $audio->status = 0;
            $audio->save();
        } catch (\Exception $e) {
            // dump($e); die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.delete') . ' ' . ucwords(__('menu.audio')));
        }

        return back()->with('success', __('wording.success') . ' ' . __('wording.delete') . ' ' . ucwords(__('menu.audio')));
    }
}
