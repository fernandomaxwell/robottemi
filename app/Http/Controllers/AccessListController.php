<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;

class AccessListController extends Controller
{
    public function __construct()
    {
        $this->dt = date('Y-m-d H:i:s');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $title = __('menu.authority');

        // Get data
        $users = User::active()
            ->admin()
            ->searchWithRole($request->keywords)
            ->paginate();

        // Append filter as query string
        $users->appends($request->except('page'));

        return view('authority.browse', compact(
            'request',
            'title',
            'users'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = __('wording.add') . ' ' . __('menu.authority');

        $roles = Role::admin()->get();

        return view('authority.add', compact(
            'title',
            'roles'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate email
        $user = User::where('email', $request->email)->first();
        if ($user) {
            return back()->withInput()->withErrors(__('errors.is_exist', ['field' => __('auth.email')]));
        }

        try {
            User::insert([
                'name' => $request->name,
                'email' => strtolower($request->email),
                'password' => \Hash::make($request->password),
                'role_id' => $request->role,
                'created_at' => $this->dt,
            ]);
        } catch (\Exception $e) {
            // dump($e); die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.add') . ' ' . ucwords(__('menu.authority')));
        }

        return redirect('authority')->with('success', __('wording.success') . ' ' . __('wording.add') . ' ' . ucwords(__('menu.authority')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = __('wording.edit') . ' ' . __('menu.authority');

        $user = User::active()->findOrFail($id);
        $roles = Role::admin()->get();

        return view('authority.edit', compact(
            'title',
            'user',
            'roles'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate email
        $user = User::where('email', $request->email)->where('id', '!=', $id)->first();
        if ($user) {
            return back()->withInput()->withErrors(__('errors.is_exist', ['field' => __('auth.email')]));
        }

        // Check if id exist
        $user = User::findOrFail($id);

        try {
            if ($request->password) {
                $user->password = \Hash::make($request->password);
            }

            $user->name = $request->name;
            $user->email = $request->email;
            $user->role_id = $request->role;
            $user->save();
        } catch (\Exception $e) {
            // dump($e);die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.edit') . ' ' . ucwords(__('menu.authority')));
        }

        return back()->with('success', __('wording.success') . ' ' . __('wording.edit') . ' ' . ucwords(__('menu.authority')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Check if id exist
        $user = User::active()->findOrFail($id);

        try {
            $user->delete();
        } catch (\Exception $e) {
            // dump($e); die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.delete'));
        }

        return back()->with('success', __('wording.success') . ' ' . __('wording.delete'));
    }
}
