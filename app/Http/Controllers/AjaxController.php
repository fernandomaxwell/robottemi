<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Order;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function __construct()
    {
        $this->brand_db = new Brand();
        $this->order_db = new Order();
    }

    public function countTransaction(Request $request)
    {
        $orders = $this->order_db->getList($request->keywords)->get();

        // Assign data to be return
        $data = [
            'total' => $orders->count(),
        ];

        return response()->json([
            'status' => 'success',
            'status_code' => 200,
            'message' => __('wording.success'),
            'items' => $data,
        ], 200);
    }

    public function countRevenue(Request $request)
    {
        $orders = $this->order_db->getList($request->keywords)->get();

        // Assign data to be return
        $data = [
            'total' => 'Rp ' . number_format($orders->sum('total_price'), '0', ',', '.') . ',-',
        ];

        return response()->json([
            'status' => 'success',
            'status_code' => 200,
            'message' => __('wording.success'),
            'items' => $data,
        ], 200);
    }

    public function setRandomVideo(Request $request)
    {
        try {
            $brand = $this->brand_db->active()->findOrFail($request->brand_id);

            $brand->random_ads = $request->random == 'true' ? true : false;
            $brand->save();

            return response()->json([
                'status' => 'success',
                'status_code' => 200,
                'message' => __('wording.success'),
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'status_code' => 422,
                'message' => __('wording.fail'),
            ], 422);
        }
    }
}
