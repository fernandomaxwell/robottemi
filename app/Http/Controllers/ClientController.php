<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->role_db = new Role();
        $this->user_db = new User();

        // Check if client's role is exist
        $this->role = $this->role_db->client()->firstOrFail();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $title = __('menu.client');

        // Get data
        $clients = $this->user_db->getListClient($request->keywords)->sortable()->paginate();

        // Append filter as query string
        $clients->appends($request->except('page'));

        return view('client.browse', compact(
            'request',
            'title',
            'clients'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort('404');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort('404');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = __('wording.view') . ' ' . __('menu.client');

        // Check if client exist
        $client = $this->user_db->client()->with('packages')->findOrFail($id);

        return view('client.show', compact(
            'title',
            'client'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort('404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Check if client exist
        $client = $this->user_db->client()->findOrFail($id);

        try {
            $client->delete();
        } catch (\Exception $e) {
            // dump($e); die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.delete') . ' ' . ucwords(__('menu.client')));
        }

        return back()->with('success', __('wording.success') . ' ' . __('wording.delete') . ' ' . ucwords(__('menu.client')));
    }
}
