<?php

namespace App\Http\Controllers;

class PageController extends Controller
{
    public function index()
    {
        abort('404');

        $title = __('menu.home');

        return view('pages.home', compact(
            'title'
        ));
    }

    public function profileManagement()
    {
        // Set title page
        $title = __('wording.profile-management');

        return view('pages.profile-management', compact(
            'title'
        ));
    }
}
