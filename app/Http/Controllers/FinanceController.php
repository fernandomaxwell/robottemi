<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\Order;
use Illuminate\Http\Request;

class FinanceController extends Controller
{
    public function __construct()
    {
        $this->order_db = new Order();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $title = __('menu.finance');

        // Get data
        $orders = $this->order_db->getList($request->keywords)->sortable()->paginate();

        // Append filter as query string
        $orders->appends($request->except('page'));

        return view('finance.browse', compact(
            'request',
            'title',
            'orders'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = __('wording.view') . ' ' . __('menu.finance');

        // Get data
        $order = $this->order_db->with(['details.package', 'client', 'status'])
            ->has('details')
            ->has('client')
            ->has('status')
            ->has('details.package')
            ->findOrFail($id);

        return view('finance.show', compact(
            'title',
            'order'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
