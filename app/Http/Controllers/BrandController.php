<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddBrandRequest;
use App\Http\Requests\EditBrandRequest;
use App\Http\Requests\SearchRequest;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->brand_db = new Brand();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $title = __('menu.brand');

        $brands = $this->brand_db->getList($request->keywords)->paginate();

        // Append filter as query string
        $brands->appends($request->except('page'));

        return view('brand.browse', compact(
            'request',
            'title',
            'brands'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = __('wording.add') . ' ' . __('menu.brand');

        return view('brand.add', compact(
            'title'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddBrandRequest $request)
    {
        // Validate username
        $brand = $this->brand_db->where('username', $request->username)->first();
        if ($brand) {
            return back()->withInput()->withErrors(__('errors.is_exist', ['field' => __('auth.username')]));
        }

        try {
            $this->brand_db->create([
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'name' => $request->name,
                'category' => $request->category,
                'is_verify' => 1, // default
                'token' => '',
                'status' => '1', // default
            ]);
        } catch (\Exception $e) {
            // dump($e); die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.add') . ' ' . ucwords(__('menu.brand')));
        }

        return redirect('brand')->with('success', __('wording.success') . ' ' . __('wording.add') . ' ' . ucwords(__('menu.brand')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = __('wording.edit') . ' ' . __('menu.brand');

        $brand = $this->brand_db->active()->findOrFail($id);

        return view('brand.edit', compact(
            'title',
            'brand'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditBrandRequest $request, $id)
    {
        // Validate email
        $brand = $this->brand_db->where('username', $request->username)->where('id', '!=', $id)->first();
        if ($brand) {
            return back()->withInput()->withErrors(__('errors.is_exist', ['field' => __('auth.username')]));
        }

        // Check if id exist
        $brand = $this->brand_db->active()->findOrFail($id);

        try {
            if ($request->password) {
                $brand->password = Hash::make($request->password);
            }

            $brand->username = $request->username;
            $brand->name = $request->name;
            $brand->category = $request->category;
            $brand->save();
        } catch (\Exception $e) {
            // dump($e);die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.edit') . ' ' . ucwords(__('menu.brand')));
        }

        return back()->with('success', __('wording.success') . ' ' . __('wording.edit') . ' ' . ucwords(__('menu.brand')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Check if id exist
        $brand = $this->brand_db->active()->findOrFail($id);

        try {
            $brand->status = 0;
            $brand->save();
        } catch (\Exception $e) {
            // dump($e); die;
            return back()->withInput()->withErrors(__('wording.fail') . ' ' . __('wording.delete') . ' ' . ucwords(__('menu.brand')));
        }

        return back()->with('success', __('wording.success') . ' ' . __('wording.delete') . ' ' . ucwords(__('menu.brand')));
    }
}
