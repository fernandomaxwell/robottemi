<?php

namespace App\Http\Middleware;

use App\Models\Language;
use App\Models\Menu;
use Closure;

class CheckUserMenu
{
    public function __construct()
    {
        $this->language_db = new Language();
        $this->menu_db = new Menu();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        if (!$user || $user->deleted_at) {
            \Auth::logout();
            return redirect('/login');
        }

        $role_menus = isset($user->role) ? (isset($user->role->menus) ? $user->role->menus : abort('404')) : abort('404');

        $language = $this->language_db->code(\App::getLocale())->first();
        $menus = $this->menu_db->getListMenu($language->id, $role_menus->pluck('id'))->get();

        $request->attributes->add(['menus' => $menus]);

        return $next($request);
    }
}
