<?php

namespace App\Http\Middleware;

use App\Helpers\Permission as PermissionHelper;
use Closure;

class CheckAuthorize
{
    public function __construct()
    {
        $this->permission_helper = new PermissionHelper();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $is_authorized = false;

        // Get requested action
        $controller = getControllerFromRoute($request->route());
        $method = getMethodFromRoute($request->route());

        $user = \Auth::user();

        if (!$user || $user->deleted_at || !$user->role) {
            \Auth::logout();
            return redirect('/login');
        }

        $is_authorized = $this->permission_helper->checkPermissionRole($controller, $method, $user->role->id);
        if (!$is_authorized) {
            \Auth::logout();
            return redirect('/login');
        }

        return $next($request);
    }
}
