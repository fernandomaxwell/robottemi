<?php

namespace App\Http\Middleware;

use App\Models\Brand;
use Closure;

class HasBrand
{
    public function __construct()
    {
        $this->brand_db = new Brand();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->brand) {
            abort('404');
        }

        $brand = $this->brand_db->select('id', 'name', 'username', 'category', 'random_ads')
            ->active()
            ->findOrFail($request->brand);

        $request->attributes->add(['brand' => $brand]);

        return $next($request);
    }
}
