<?php

namespace App\Http\Middleware;

use App\Models\Language;
use App\Models\Setting;
use Closure;

class GetSetting
{
    public function __construct()
    {
        $this->language = session('language', config('app.locale'));

        $this->language_db = new Language();
        $this->setting_db = new Setting();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Set language
        \App::setLocale($this->language);

        $request->attributes->add([
            'logo_dark' => $this->setting_db->getLogoDark(),
            'logo_white' => $this->setting_db->getLogoWhite(),
            'languages' => $this->language_db->get(),
            'default_redirect_to' => $this->setting_db->getDefaultRedirect(),
            'asset_version' => $this->setting_db->getAssetVersion(),
        ]);
        return $next($request);
    }
}
