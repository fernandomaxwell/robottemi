<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    /**
     * Relation
     * https://laravel.com/docs/5.8/eloquent-relationships
     */
     public function package()
     {
         return $this->belongsTo('App\Models\Package', 'package_id');
     }
}
