<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * Query
     */
    public function getLogoDark()
    {
        $logo = $this->where('key', 'logo_dark')->first();
        return $logo ? $logo->value : config('app.logo');
    }

    public function getLogoWhite()
    {
        $logo = $this->where('key', 'logo_white')->first();
        return $logo ? $logo->value : config('app.logo');
    }

    public function getDefaultRedirect()
    {
        $redirect_to = $this->where('key', 'default_redirect_to')->first();
        return $redirect_to ? $redirect_to->value : '/';
    }

    public function getAssetVersion()
    {
        $asset_version = $this->where('key', 'asset_version')->first();
        return $asset_version ? $asset_version->value : '';
    }
}
