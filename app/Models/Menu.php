<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * Accessor
     * https://laravel.com/docs/5.8/eloquent-mutators#defining-an-accessor
     */
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    /**
     * Relation
     * https://laravel.com/docs/5.8/eloquent-relationships
     */
    public function children()
    {
        return $this->hasMany('App\Models\Menu', 'parent_id');
    }

    /**
     * Scope
     * https://laravel.com/docs/7.x/eloquent#local-scopes
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function scopeParent($query)
    {
        return $query->whereNull('parent_id');
    }

    public function scopeLanguage($query, $language)
    {
        return $query->where('lang_id', $language);
    }

    /**
     * Query
     */
    public function getListMenu($language, $menu_ids = null)
    {
        return $this->parent()
            ->active()
            ->language($language)
            ->when($menu_ids, function ($menu) use ($menu_ids) {
                $menu->whereIn('id', $menu_ids);
            })
            ->with('children');
    }
}
