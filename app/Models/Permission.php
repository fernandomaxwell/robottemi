<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * Relation
     * https://laravel.com/docs/5.8/eloquent-relationships
     */
     public function roles()
     {
         return $this->belongsToMany('App\Models\Role', 'permission_roles');
     }
 
     /**
      * Query
      */
     public function getPermissionByControllerAndMethod($controller, $method)
     {
         return $this->where([
             'controller' => $controller,
             'method' => $method,
         ]);
     }
 
     public function getPermissionRole($controller, $method, $role_id)
     {
         return $this->getPermissionByControllerAndMethod($controller, $method)
             ->whereHas('roles', function ($role) use ($role_id) {
                 $role->where('role_id', $role_id);
             })->first();
     }
}
