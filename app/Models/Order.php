<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Order extends Model
{
    use Sortable;

    /**
     * sortable
     * https://github.com/Kyslik/column-sortable
     */
    public $sortable = ['created_at', 'total_price'];

    /**
     * Relation
     * https://laravel.com/docs/5.8/eloquent-relationships
     */
    public function details()
    {
        return $this->hasMany('App\Models\OrderDetail', 'order_id');
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\OrderStatus', 'status_id');
    }

    /**
     * Query
     */
    public function getList($keywords)
    {
        return $this->with(['client', 'status'])
            ->has('client')
            ->has('status')
            ->when($keywords, function ($order) use ($keywords) {
                $order->where('orders.created_at', 'LIKE', '%' . $keywords . '%')
                    ->orWhere('total_price', 'LIKE', '%' . $keywords . '%')
                    ->orWhereHas('client', function ($client) use ($keywords) {
                        $client->where('name', 'LIKE', '%' . $keywords . '%')
                            ->orWhere('email', 'LIKE', '%' . $keywords . '%');
                    })
                    ->orWhereHas('status', function ($status) use ($keywords) {
                        $status->where('name', 'LIKE', '%' . $keywords . '%');
                    });
            });
    }
}
