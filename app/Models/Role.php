<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * Relation
     * https://laravel.com/docs/5.8/eloquent-relationships
     */
    public function menus()
    {
        return $this->belongsToMany('App\Models\Menu', 'role_menus');
    }

    /**
     * Scope
     * https://laravel.com/docs/5.8/eloquent#local-scopes
     */
    public function scopeAdmin($query)
    {
        return $query->where('name', 'admin');
    }

    public function scopeClient($query)
    {
        return $query->where('name', 'client');
    }
}
