<?php

namespace App\Models;

use App\Scopes\CreatedByScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Brand extends Model
{
    protected $table = 'auth';
    public $timestamps = false;
    
    /**
     * Guarded fields
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CreatedByScope);

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }

    /**
     * Accessor
     * https://laravel.com/docs/5.8/eloquent-mutators#defining-an-accessor
     */
    public function getUsernameAttribute($value)
    {
        return strtolower($value);
    }

    /**
     * Relation
     * https://laravel.com/docs/5.8/eloquent-relationships
     */
    public function videos()
    {
        return $this->hasMany('App\Models\Advertisement', 'user_id');
    }

    public function audios()
    {
        return $this->hasMany('App\Models\Audio', 'user_id');
    }

    /**
     * Scope
     * https://laravel.com/docs/5.8/eloquent#local-scopes
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeSearch($query, $keywords)
    {
        return $query->where('name', 'LIKE', '%' . $keywords . '%')
            ->orWhere('username', 'LIKE', '%' . $keywords . '%')
            ->orWhere('category', 'LIKE', '%' . $keywords . '%');
    }

    /**
     * Query
     */
    public function getList($keywords = null)
    {
        return $this->active()
            ->when($keywords, function ($brand) use ($keywords) {
                $brand->search($keywords);
            });
    }
}
