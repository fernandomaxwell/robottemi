<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class LeadGeneration extends Model
{
    use Sortable;

    /**
     * sortable
     * https://github.com/Kyslik/column-sortable
     */
    public $sortable = ['name', 'email', 'company', 'whatsapp_number'];

    /**
     * Accessor
     * https://laravel.com/docs/5.8/eloquent-mutators#defining-an-accessor
     */
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function getEmailAttribute($value)
    {
        return strtolower($value);
    }

    public function getWhatsappNumberAttribute($value)
    {
        return $value[0] == '0' ? substr($value, 1) : $value;
    }

    /**
     * Query
     */
    public function getList($keywords)
    {
        return $this->when($keywords, function ($lead_generation) use ($keywords) {
            $lead_generation->where('name', 'LIKE', '%' . $keywords . '%')
                ->orWhere('email', 'LIKE', '%' . $keywords . '%')
                ->orWhere('company', 'LIKE', '%' . $keywords . '%')
                ->orWhere('whatsapp_number', 'LIKE', '%' . $keywords . '%');
        });
    }
}
