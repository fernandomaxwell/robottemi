<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class OrderStatus extends Model
{
    use Sortable;

    /**
     * sortable
     * https://github.com/Kyslik/column-sortable
     */
    public $sortable = ['name'];

    /**
     * Accessor
     * https://laravel.com/docs/5.8/eloquent-mutators#defining-an-accessor
     */
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }
}
