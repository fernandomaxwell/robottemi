<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * Scope
     * https://laravel.com/docs/7.x/eloquent#local-scopes
     */
    public function scopeCode($query, $code)
    {
        return $query->where('code', $code);
    }
}
