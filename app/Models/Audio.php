<?php

namespace App\Models;

use App\Scopes\CreatedByScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Audio extends Model
{
    /**
     * Guarded fields
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CreatedByScope);

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }

    /**
     * Scope
     * https://laravel.com/docs/5.8/eloquent#local-scopes
     */
    public function scopeBrand($query, $brand_id)
    {
        return $query->where('user_id', $brand_id);
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeSearch($query, $keywords)
    {
        return $query->where('name', 'LIKE', '%' . $keywords . '%');
    }

    /**
     * Query
     */
    public function getList($brand_id, $type, $keywords = null)
    {
        return $this->brand($brand_id)
            ->type($type)
            ->active()
            ->when($keywords, function ($audio) use ($keywords) {
                $audio->search($keywords);
            });
    }

    public function getLatestPriority($brand_id)
    {
        return $this->brand($brand_id)->latest('priority');
    }
}
