<?php

namespace App\Models;

use App\Scopes\CreatedByScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Advertisement extends Model
{
    /**
     * Guarded fields
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CreatedByScope);

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }

    /**
     * Scope
     * https://laravel.com/docs/5.8/eloquent#local-scopes
     */
    public function scopeBrand($query, $brand_id)
    {
        return $query->where('user_id', $brand_id);
    }

    public function scopeType($query, $type)
    {
        if ($type) {
            return $query->where('type', $type);
        }

        return $query;
    }

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function scopeSearch($query, $keywords)
    {
        return $query->where('name', 'LIKE', '%' . $keywords . '%')
            ->orWhere('advertiser', 'LIKE', '%' . $keywords . '%')
            ->orWhere('type_of_advertiser_company', 'LIKE', '%' . $keywords . '%');
    }

    /**
     * Query
     */
    public function getList($brand_id, $type = null, $keywords = null)
    {
        return $this->brand($brand_id)
            ->type($type)
            ->active()
            ->when($keywords, function ($advertisement) use ($keywords) {
                $advertisement->search($keywords);
            })
            ->orderByRaw('-priority DESC');
    }

    public function setNullPriority($priority, $brand_id)
    {
        $this->brand($brand_id)
            ->where('priority', $priority)
            ->update([
                'priority' => null,
            ]);
    }
}
