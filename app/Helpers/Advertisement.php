<?php

namespace App\Helpers;

use App\Models\Advertisement as AdvertisementDb;

class Advertisement
{
    public function __construct()
    {
        $this->advertisement_db = new AdvertisementDb();

        $this->max_priority = 5;
        $this->base_dir = 'public/video';
    }

    public function uploadVideo($video)
    {
        $path = null;
        if ($video && $video->isValid()) {
            $path = $video->store($this->base_dir);
        }

        return $path ? url('/') . explode('public/', $path)[0] . '/storage/' . explode('public/', $path)[1] : $path;
    }

    public function deleteVideo($url)
    {
        $arr_url = explode('/', $url);
        $file = array_pop($arr_url);
        \Storage::delete($this->base_dir . '/' . $file);
    }

    public function setTotalPriority($brand)
    {
        $total_advertisements = $this->advertisement_db->getList($brand->id)->count();
        return $total_advertisements > $this->max_priority ? $this->max_priority : $total_advertisements;
    }
}
