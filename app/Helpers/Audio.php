<?php

namespace App\Helpers;

use App\Models\AudioType;

class Audio
{
    public function __construct()
    {
        $this->base_dir = 'public/audio';
    }

    public function uploadAudio($audio)
    {
        $path = null;
        if ($audio && $audio->isValid()) {
            $path = $audio->store($this->base_dir);
        }
        return $path ? url('/') . explode('public/', $path)[0] . '/storage/' . explode('public/', $path)[1] : $path;
    }

    public function deleteAudio($url)
    {
        $arr_url = explode('/', $url);
        $file = array_pop($arr_url);
        \Storage::delete($this->base_dir . '/' . $file);
    }

    public function validateType($type)
    {
        AudioType::where('name', $type)->firstOrFail();
    }
}
