<?php

namespace App\Helpers;

use App\Models\Permission as PermissionDb;

class Permission
{
    public function __construct()
    {
        $this->permission_db = new PermissionDb();
    }

    public function checkPermissionRole($controller, $method, $role_id)
    {
        $permission = $this->permission_db->getPermissionRole($controller, $method, $role_id);
        return $permission ? true : false;
    }
}
