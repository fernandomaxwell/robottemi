<?php

function getRouteFromUrl($url)
{
    return Route::getRoutes()->match(app('request')->create($url));
}

function getNameSpaceFromRoute($route)
{
    $action = class_basename($route->getActionname());
    $namespace = explode('@', $action);
    return $namespace;
}

function getControllerFromRoute($route)
{
    $namespace = getNameSpaceFromRoute($route);
    $controller = $namespace[0];
    return $controller;
}

function getMethodFromRoute($route)
{
    $namespace = getNameSpaceFromRoute($route);
    $method = end($namespace);
    return $method;
}