<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguageSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(RoleMenuSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(PermissionRoleSeeder::class);
        $this->call(OrderStatusSeeder::class);
        // $this->call(AudioSeeder::class);
        $this->call(AudioTypeSeeder::class);

        $this->call(MappingAuthSeeder::class);
        $this->call(MappingAudioSeeder::class);
        $this->call(MappingAdvertisementSeeder::class);
    }
}
