<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    public function __construct()
    {
        $this->dt = date('Y-m-d H:i:s');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate
        Role::truncate();

        // Mapping
        $data = [
            [
                'name' => 'super admin',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'admin',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
        ];

        // Insert
        Role::insert($data);
    }
}
