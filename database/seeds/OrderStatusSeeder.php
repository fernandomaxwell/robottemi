<?php

use App\Models\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    public function __construct()
    {
        $this->dt = date('Y-m-d H:i:s');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Trunccate
        OrderStatus::truncate();

        // Mapping
        $data = [
            [
                'name' => 'pending',
                'sort' => 1,
                'created_at' => $this->dt,
                'updated_at' => $this->dt, 
            ],
            [
                'name' => 'confirmed',
                'sort' => 2,
                'created_at' => $this->dt,
                'updated_at' => $this->dt, 
            ],
        ];

        // Insert
        OrderStatus::insert($data);
    }
}
