<?php

use App\Models\Menu;
use App\Models\Role;
use App\Models\RoleMenu;
use Illuminate\Database\Seeder;

class RoleMenuSeeder extends Seeder
{
    public function __construct()
    {
        $this->dt = date('Y-m-d H:i:s');
        $this->role_menu_links = [
            'super admin' => [
                'brand',
                'advertisement',
                'audio',
                'profile-management',
                'authority',
            ],
            'admin' => [
                'brand',
                'advertisement',
                'audio',
                'profile-management',
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate
        RoleMenu::truncate();

        // Data
        $data = [];
        $roles = Role::get();
        foreach ($roles as $role) {
            $links = $this->role_menu_links[$role->name];
            $menus = Menu::whereIn('link', $links)->get();

            foreach ($menus as $menu) {
                $data[] = [
                    'role_id' => $role->id,
                    'menu_id' => $menu->id,
                    'created_at' => $this->dt,
                ];
            }
        }

        // Insert
        RoleMenu::insert($data);
    }
}
