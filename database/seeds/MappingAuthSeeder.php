<?php

use App\Models\Brand;
use App\User;
use Illuminate\Database\Seeder;

class MappingAuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'temiindonesia@gmail.com')->first();
        if ($user) {
            Brand::where('created_by', 0)->update([
                'created_by' => $user->id,
            ]);
        }
    }
}
