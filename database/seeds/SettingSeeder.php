<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    public function __construct()
    {
        $this->dt = date('Y-m-d H:i:s');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate
        Setting::truncate();

        // Mapping
        $data = [
            [
                'key' => 'logo_dark',
                'value' => 'img/logo_dark.svg',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'key' => 'logo_white',
                'value' => 'img/logo_white.png',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'key' => 'default_redirect_to',
                'value' => 'brand',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'key' => 'asset_version',
                'value' => '1.0.0',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
        ];

        // Insert
        Setting::insert($data);
    }
}
