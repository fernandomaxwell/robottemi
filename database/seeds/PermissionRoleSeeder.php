<?php

use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    public function __construct()
    {
        $this->permission_db = new Permission();

        $this->dt = date('Y-m-d H:i:s');
        $this->resource_methods = ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy'];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate
        PermissionRole::truncate();

        // Data
        $data = [];
        $roles = Role::with('menus')->get();
        foreach ($roles as $role) {
            // Based on ajax controller
            $ajax_permissions = Permission::where('controller', 'AjaxController')->get();
            foreach ($ajax_permissions as $permission) {
                $data[] = [
                    'role_id' => $role->id,
                    'permission_id' => $permission->id,
                    'created_at' => $this->dt,
                ];
            }

            // Based on setting controller
            $setting_permissions = Permission::where('controller', 'SettingController')->get();
            foreach ($setting_permissions as $permission) {
                $data[] = [
                    'role_id' => $role->id,
                    'permission_id' => $permission->id,
                    'created_at' => $this->dt,
                ];
            }

            // Based on change password methos
            $change_password_permission = Permission::where('method', 'changePassword')->first();
            if ($change_password_permission) {
                $data[] = [
                    'role_id' => $role->id,
                    'permission_id' => $change_password_permission->id,
                    'created_at' => $this->dt,
                ];
            }

            // Based on export controller
            $export_permissions = Permission::where('method', 'export')->get();
            foreach ($export_permissions as $permission) {
                $data[] = [
                    'role_id' => $role->id,
                    'permission_id' => $permission->id,
                    'created_at' => $this->dt,
                ];
            }

            // Based on role menu (just choose one language)
            foreach ($role->menus->where('lang_id', 1) as $menu) {
                if ($menu->children->isEmpty()) {
                    $route = getRouteFromUrl($menu->link);
                    $controller = getControllerFromRoute($route);

                    if ($menu->is_resource) {
                        foreach ($this->resource_methods as $resource_method) {
                            $permission = $this->permission_db->getPermissionByControllerAndMethod($controller, $resource_method)->first();

                            if ($permission) {
                                $data[] = [
                                    'role_id' => $role->id,
                                    'permission_id' => $permission->id,
                                    'created_at' => $this->dt,
                                ];
                            }
                        }
                    } else {
                        $method = getMethodFromRoute($route);
                        $permission = $this->permission_db->getPermissionByControllerAndMethod($controller, $method)->first();

                        if ($permission) {
                            $data[] = [
                                'role_id' => $role->id,
                                'permission_id' => $permission->id,
                                'created_at' => $this->dt,
                            ];
                        }
                    }
                } else {
                    foreach ($menu->children as $child) {
                        $route = getRouteFromUrl($menu->link);
                        $controller = getControllerFromRoute($route);

                        if ($child->is_resource) {
                            foreach ($this->resource_methods as $resource_method) {
                                $permission = $this->permission_db->getPermissionByControllerAndMethod($controller, $resource_method)->first();

                                if ($permission) {
                                    $data[] = [
                                        'role_id' => $role->id,
                                        'permission_id' => $permission->id,
                                        'created_at' => $this->dt,
                                    ];
                                }
                            }
                        } else {
                            $method = getMethodFromRoute($route);
                            $permission = $this->permission_db->getPermissionByControllerAndMethod($controller, $method)->first();

                            if ($permission) {
                                $data[] = [
                                    'role_id' => $role->id,
                                    'permission_id' => $permission->id,
                                    'created_at' => $this->dt,
                                ];
                            }
                        }
                    }
                }
            }
        }

        // Insert
        PermissionRole::insert($data);
    }
}
