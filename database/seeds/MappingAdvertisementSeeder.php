<?php

use App\Models\{
    Advertisement,
    AudioType,
};
use App\User;
use Illuminate\Database\Seeder;

class MappingAdvertisementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'temiindonesia@gmail.com')->first();
        if ($user) {
            Advertisement::where('created_by', 0)->update([
                'created_by' => $user->id,
            ]);
        }

        $type = AudioType::where('name', 'greetings')->first();
        if ($type) {
            Advertisement::where('type_id', 0)->update([
                'type_id' => $type->id,
                'type' => $type->name,
            ]);
        }
    }
}
