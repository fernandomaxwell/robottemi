<?php

use App\Models\Audio;
use App\User;
use Illuminate\Database\Seeder;

class MappingAudioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'temiindonesia@gmail.com')->first();
        if ($user) {
            Audio::where('created_by', 0)->update([
                'created_by' => $user->id,
            ]);
        }
    }
}
