<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    public function __construct()
    {
        $this->dt = date('Y-m-d H:i:s');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate
        Permission::truncate();

        // Data
        $data = [];
        foreach (Route::getRoutes()->getRoutes() as $route) {
            $controller = getControllerFromRoute($route);
            $method = getMethodFromRoute($route);

            $data[] = [
                'controller' => $controller,
                'method' => $method,
                'created_at' => $this->dt,
            ];
        }

        // Insert
        Permission::insert($data);
    }
}
