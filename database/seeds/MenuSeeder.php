<?php

use App\Models\Language;
use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    public function __construct()
    {
        $this->dt = date('Y-m-d H:i:s');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate
        Menu::truncate();

        // Get available language
        $languages = Language::get();

        // Mapping Data
        $data = [
            [
                'name' => 'ringkasan dasbor',
                'link' => '/',
                'icon' => 'fa fa-home',
                'lang_id' => $languages->where('code', 'id')->first()->id,
                'parent_id' => null,
                'is_resource' => 0,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'overview dashboard',
                'link' => '/',
                'icon' => 'fa fa-home',
                'lang_id' => $languages->where('code', 'en')->first()->id,
                'parent_id' => null,
                'is_resource' => 0,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'manajemen brand',
                'link' => 'brand',
                'icon' => 'img/menu-icon/brand.png',
                'lang_id' => $languages->where('code', 'id')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'active',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'brand management',
                'link' => 'brand',
                'icon' => 'img/menu-icon/brand.png',
                'lang_id' => $languages->where('code', 'en')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'active',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'manajemen iklan',
                'link' => 'advertisement',
                'icon' => 'fa fa-film',
                'lang_id' => $languages->where('code', 'id')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'ads management',
                'link' => 'advertisement',
                'icon' => 'fa fa-film',
                'lang_id' => $languages->where('code', 'en')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'manajemen suara',
                'link' => 'audio',
                'icon' => 'fa fa-music',
                'lang_id' => $languages->where('code', 'id')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'sound management',
                'link' => 'audio',
                'icon' => 'fa fa-music',
                'lang_id' => $languages->where('code', 'en')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'manajemen klien',
                'link' => 'client',
                'icon' => 'fa fa-users',
                'lang_id' => $languages->where('code', 'id')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'client management',
                'link' => 'client',
                'icon' => 'fa fa-users',
                'lang_id' => $languages->where('code', 'en')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'monitor keuangan dasar',
                'link' => 'finance',
                'icon' => 'fa fa-book',
                'lang_id' => $languages->where('code', 'id')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'basic finance monitor',
                'link' => 'finance',
                'icon' => 'fa fa-book',
                'lang_id' => $languages->where('code', 'en')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'otoritas dan peran manajemen',
                'link' => 'authority',
                'icon' => 'img/menu-icon/authority.png',
                'lang_id' => $languages->where('code', 'id')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'active',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'authority & role management',
                'link' => 'authority',
                'icon' => 'img/menu-icon/authority.png',
                'lang_id' => $languages->where('code', 'en')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'active',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'lead generation',
                'link' => 'lead-generation',
                'icon' => 'fa fa-child',
                'lang_id' => $languages->where('code', 'id')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'lead generation',
                'link' => 'lead-generation',
                'icon' => 'fa fa-child',
                'lang_id' => $languages->where('code', 'en')->first()->id,
                'parent_id' => null,
                'is_resource' => 1,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'profile management',
                'link' => 'profile-management',
                'icon' => '',
                'lang_id' => $languages->where('code', 'id')->first()->id,
                'parent_id' => null,
                'is_resource' => 0,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'profile management',
                'link' => 'profile-management',
                'icon' => '',
                'lang_id' => $languages->where('code', 'en')->first()->id,
                'parent_id' => null,
                'is_resource' => 0,
                'status' => 'inactive',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
        ];

        // Insert
        Menu::insert($data);
    }
}
