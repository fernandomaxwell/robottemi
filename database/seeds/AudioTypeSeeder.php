<?php

use App\Models\AudioType;
use Illuminate\Database\Seeder;

class AudioTypeSeeder extends Seeder
{
    public function __construct()
    {
        $this->dt = date('Y-m-d H:i:s');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AudioType::truncate();

        $data = [
            [
                'name' => 'greetings',
                'created_at' => $this->dt,
            ],
            [
                'name' => 'patrol',
                'created_at' => $this->dt,
            ],
        ];

        AudioType::insert($data);
    }
}
