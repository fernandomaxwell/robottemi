<?php

use App\Models\Audio;
use Illuminate\Database\Seeder;

class AudioSeeder extends Seeder
{
    public function __construct()
    {
        $this->dt = date('Y-m-d H:i:s');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Audio::get()->isEmpty()) {
            echo 'audio exist';
            return;
        }

        // Truncate
        Audio::truncate();

        // Mapping data
        $data = [
            [
                'name' => 'opening (facing customer)',
                'path' => '',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'opening (gender male/man)',
                'path' => '',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'opening (gender female/woman)',
                'path' => '',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
            [
                'name' => 'closing',
                'path' => '',
                'created_at' => $this->dt,
                'updated_at' => $this->dt,
            ],
        ];

        // Insert
        Audio::insert($data);
    }
}
