<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAuth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auth', function (Blueprint $table) {
            $table->string('name')->nullable()->after('password');
            $table->string('category')->nullable()->after('name');
            $table->boolean('random_ads')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auth', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('category');
            $table->dropColumn('random_ads');
        });
    }
}
