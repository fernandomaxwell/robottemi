<?php

return [
    'required' => ':field is required',
    'not_valid' => ':field is not valid',
    'not_found' => ':field is not found',
    'is_exist' => ':field is already exist',
    'min' => ':field minimum :min',
    'max' => ':field maximum :max',
];
