<?php

return [
    'home' => 'Overview Dashboard',
    'brand' => 'Brand Management',
    'audio' => 'Sound Management',
    'advertisement' => 'Ads Management',
    'client' => 'Client Management',
    'finance' => 'Basic Finance Monitor',
    'authority' => 'Authority & Role',
    'lead_generation' => 'Lead Generation',
];
