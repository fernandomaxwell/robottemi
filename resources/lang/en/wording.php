<?php

return [
    'add' => 'Add',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'view' => 'View',
    'upload' => 'Upload',
    'download' => 'Download',
    'input' => 'Input',
    'submit' => 'Submit',
    'back' => 'Back',
    'search' => 'Search',
    'fail' => 'Fail',
    'success' => 'Success',
    'copy' => 'Copy',
    'choose' => 'Choose',
    'show' => 'Show',

    'required' => 'Required',
    'name' => 'Name',
    'typed' => 'Type',
    'something' => 'Something',
    'list' => 'List',
    'action' => 'Action',
    'file' => 'File',
    'audio' => 'Audio',
    'video' => 'Video',
    'priority' => 'Priority',
    'copyright' => 'Copyright',
    'setting' => 'Setting',
    'profile_management' => 'Profile Management',
    'on_progress' => 'Feature still in progress',
    'company' => 'Company',
    'confirm_delete' => 'Are you sure want to delete this item?',
    'package' => 'Package',
    'purchase_date' => 'Purchase Date',
    'expired_date' => 'Expired Date',
    'status' => 'Status',
    'value' => 'Value',
    'quantity' => 'Quantity',
    'price' => 'Price',
    'transaction' => 'Transaction',
    'revenue' => 'Revenue',
    'type' => 'Type',
    'role' => 'Role',

    'brand' => [
        'name' => 'Brand Name',
        'category' => 'Brand Category',
    ],

    'ads' => [
        'name' => 'Ads Name',
        'advertiser' => 'Advertiser',
        'video' => 'Ads Video',
        'company_type' => 'Advertiser Type',
        'play_video_random' => 'Play Video Random',
    ],

    'client' => [
        'name' => 'Client Name',
        'email' => 'Client E-mail',
        'company' => 'Client Company',
        'personal_info' => 'Personal Info',
    ],

    'order' => [
        'order_info' => 'Order Info',
        'detail' => 'Order Detail',
    ],

    'lead_generation' => [
        'whatsapp' => 'Whatsapp Number',
    ],

    'table' => [
        'empty' => 'No data available in table',
    ],
    'form' => [
        'audio' => 'Form Audio',
        'advertisement' => 'Form Advertisement',
        'brand' => 'Form Brand',
        'authority' => 'Form Authority',
    ],
];
