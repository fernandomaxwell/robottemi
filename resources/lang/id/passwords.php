<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
     */

    'password' => 'Kata sandi minimal harus delapan karakter dan sesuai dengan konfirmasi.',
    'reset' => 'Kata sandi telah diatur ulang!',
    'sent' => 'Tautan atur ulang kata sandi sudah dikirim ke e-mail Anda!',
    'token' => 'Token atur ulang kata sandi ini tidak valid.',
    'user' => "Email tidak ditemukan.",

];
