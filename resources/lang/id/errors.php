<?php

return [
    'required' => ':field harus diisi',
    'not_valid' => ':field tidak valid',
    'not_found' => 'field tidak ditemukan',
    'is_exist' => ':field sudah terdaftar',
    'min' => ':field minimal :min',
    'max' => ':field maksimal :max',
];
