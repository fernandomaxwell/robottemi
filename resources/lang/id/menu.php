<?php

return [
    'home' => 'Ringkasan Dasbor',
    'brand' => 'Manajemen Brand',
    'audio' => 'Manajemen Suara',
    'advertisement' => 'Manajemen Iklan',
    'client' => 'Manajemen Klien',
    'finance' => 'Monitor Keuangan Dasar',
    'authority' => 'Otoritas & Peran',
    'lead_generation' => 'Lead Generation',
];
