<?php

return [
    'add' => 'Tambah',
    'edit' => 'Ubah',
    'delete' => 'Hapus',
    'view' => 'Lihat',
    'upload' => 'Unggah',
    'download' => 'Unduh',
    'input' => 'Masukkan',
    'submit' => 'Kirim',
    'back' => 'Kembali',
    'search' => 'Cari',
    'fail' => 'Gagal',
    'success' => 'Berhasil',
    'copy' => 'Salin',
    'choose' => 'Pilih',
    'show' => 'Tampilkan',

    'required' => 'Wajib diisi',
    'name' => 'Nama',
    'typed' => 'Ketikkan',
    'something' => 'Sesuatu',
    'list' => 'Daftar',
    'action' => 'Aksi',
    'file' => 'File',
    'audio' => 'Audio',
    'video' => 'Video',
    'priority' => 'Prioritas',
    'copyright' => 'Hak Cipta',
    'setting' => 'Pengaturan',
    'profile_management' => 'Pengaturan Profil',
    'on_progress' => 'Fitur masih dalam proses pengerjaan',
    'company' => 'Perusahaan',
    'confirm_delete' => 'Apakah Anda yakin ingin menghapus ini?',
    'package' => 'Paket',
    'purchase_date' => 'Tanggal Pembelian',
    'expired_date' => 'Tanggal Kadaluarsa',
    'status' => 'Status',
    'value' => 'Nilai',
    'quantity' => 'Kuantitas',
    'price' => 'Harga',
    'transaction' => 'Transaksi',
    'revenue' => 'Pendapatan',
    'type' => 'Tipe',
    'role' => 'Peranan',

    'brand' => [
        'name' => 'Nama Brand',
        'category' => 'Kategori Brand',
    ],

    'ads' => [
        'name' => 'Nama Iklan',
        'advertiser' => 'Pengiklan',
        'video' => 'Video Iklan',
        'company_type' => 'Tipe Perusahaan Pengiklan',
        'play_video_random' => 'Acak Putar Video',
    ],

    'client' => [
        'name' => 'Nama Klien',
        'email' => 'E-mail Klien',
        'company' => 'Nama Perusahaan',
        'personal_info' => 'Data Pribadi',
    ],

    'order' => [
        'order_info' => 'Data Order',
        'detail' => 'Detil Order',
    ],

    'lead_generation' => [
        'whatsapp' => 'Nomor Whatsapp',
    ],

    'table' => [
        'empty' => 'Saat ini data tidak tersedia',
    ],
    'form' => [
        'audio' => 'Formulir Audio',
        'advertisement' => 'Formulir Iklan',
        'brand' => 'Formulir Brand',
        'authority' => 'Form Otoritas',
    ],
];
