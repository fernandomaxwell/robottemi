<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'failed' => 'E-mail atau kata sandi salah.',
    'throttle' => 'Terlalu banyak upaya masuk gagal. Silakan coba lagi dalam :seconds detik.',
    'email' => 'E-mail',
    'password' => 'Kata Sandi',
    'create_account' => 'Buat Akun',
    'forgot_password' => 'Lupa Kata Sandi',
    'confirm_password' => 'Konfirmasi Kata Sandi',
    'reset_password' => 'Atur Ulang Kata Sandi',
    'old_password' => 'Kata Sandi Lama',
    'login' => 'Masuk',
    'logout' => 'Keluar',
    'auth' => 'Daftar',
    'username' => 'Username',

];
