@extends('layouts.master', ['title' => $title])

@section('css')
    @include('style.general')
@endsection

@section('content')
    {{-- Page Heading --}}
    <div class='row pb-3'>
        <div class='col-md-6'>
            <a href='{{ url("audio?brand=" . $brand->id) }}' class='btn btn-outline-secondary'>
                <i class='fa fa-arrow-circle-left'></i> {{ __('wording.back') }}
            </a>
        </div>
    </div>

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    {{-- Form --}}
    <form method='POST' action="{{ url('audio/' . $audio->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <input type='hidden' name='brand' value='{{ $brand->id }}'>

        <div class="card shadow mb-3">
            {{-- <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">{{ __('wording.form.audio') }}</h6>
            </div> --}}

            <div class='card-body rounded bg-general'>
                {{-- Brand Name --}}
                <div class='mb-4'>
                    <span class='h5'>{{ strtoupper($brand->name ? $brand->name : $brand->username) }} |</span>
                    <span><small>{{ strtolower(__('wording.video')) }}</small></span>
                </div>

                <div class='row'>
                    <div class='col-md-6'>
                        <div class="form-group">
                            <label>{{ __('wording.name') }}:</label>
                            <input type="text" class="form-control" name='name' value="{{ old('name') ? old('name') : $audio->name }}" maxlength='255' required>
                        </div>

                        <div class='form-group'>
                            <label>{{ __('wording.audio') }}:</label>
                            <input type='file' class='form-control-file' accept="audio/mp3" name='audio' id='audio' autofocus>
                            <div class='my-2'>
                                @if ($audio->path)
                                    @if (strpos($audio->path, 'http') !== false)
                                        <audio id='audio-preview' controls>
                                            <source src="{{ $audio->path }}" type="audio/mp3">
                                        <audio>
                                    @else
                                        <audio id='audio-preview' controls>
                                            <source src="{{ asset('storage/' . $audio->path) }}" type="audio/mp3">
                                        <audio>
                                    @endif
                                @else
                                    <audio id='audio-preview' controls>
                                        <source type="audio/mp3">
                                    <audio>
                                @endif
                            </div>
                        </div>

                        <div class='form-group'>
                            <label>{{ __('wording.type') }}:</label>
                            <select class='form-control' name='type' required>
                                @php
                                    $old_type = old('type') ? old('type') : $audio->type_id;
                                @endphp
                                @foreach($audio_types as $audio_type)
                                    <option value='{{ $audio_type->id }}' @if($old_type == $audio_type->id) selected @endif>
                                        {{ $audio_type->name }}
                                    </audio>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-success mb-4">{{ __('wording.submit') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('javascript')
    <script src="{{ asset('js/audio-preview.js') }}"></script>
@endsection