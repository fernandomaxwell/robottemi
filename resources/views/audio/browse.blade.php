@extends('layouts.master', ['title' => $title])

@section('css')
    @include('style.general')
@endsection

@section('content')
    {{-- Page Heading --}}
    @include('layouts.button-add')

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">{{ __('wording.list') }} {{ ucwords($title) }}</h6>
        </div> --}}

        <div class="card-body rounded bg-video">
            {{-- Brand Name --}}
            <div class='mb-4'>
                <span class='h5'>{{ strtoupper($brand->name ? $brand->name : $brand->username) }} |</span>
                <span><small>{{ strtolower(__('wording.audio')) }}</small></span>
            </div>

            <div class='rounded'>
                <ul class="nav nav-tabs">
                    @foreach ($audio_types as $audio_type)
                        <li class="nav-item">
                            <a class="nav-link @if($audio_type->name == $request->type) active @endif" href="{{ url()->current() . '?brand=' . $brand->id . '&type=' . $audio_type->name }}">
                                {{ $audio_type->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content bg-table">
                    @foreach ($audio_types as $audio_type)
                        <div class="tab-pane container py-4 @if($audio_type->name == $request->type) active @else hide @endif">
                            {{-- Filter --}}
                            <div class='row'>
                                <div class='col-lg-6'>
                                    <form method='GET' action='{{ url()->current() }}' id='form-search'>
                                        {{-- Keywords --}}
                                        @include('layouts.keyword')
                                    </form>
                                </div>
                            </div>

                            {{-- Table --}}
                            <div class='table-responsive'>
                                <table class='table table-hover table-bordered'>
                                    <thead>
                                        <th>No.</th>
                                        <th style='min-width: 200px;'>{{ __('wording.name') }}</th>
                                        <th style='min-width: 200px;'>{{ __('wording.audio') }}</th>
                                        <th style='min-width: 150px;'>{{ __('wording.action') }}</th>
                                    </thead>

                                    <tbody>
                                        @if(isset($audios) && $audios->count() > 0)
                                            @php
                                                $no = (($audios->currentpage() - 1) * $audios->perpage()) + 1;
                                            @endphp
                                            @foreach($audios as $audio)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $audio->name }}</td>
                                                    <td>
                                                        @if($audio->path)
                                                            @if (strpos($audio->path, 'http') !== false)
                                                                <audio controls>
                                                                    <source src="{{ $audio->path }}" type="audio/mp3">
                                                                </audio>
                                                            @else
                                                                <audio controls>
                                                                    <source src="{{ asset('storage/' . $audio->path) }}" type="audio/mp3">
                                                                </audio>
                                                            @endif
                                                        @else - @endif
                                                    </td>
                                                    <td>
                                                        <form method="GET" action="{{ url()->current() . '/' . $audio->id . '/edit' }}">
                                                            <input type='hidden' name='brand' value='{{ $brand->id }}'>
                                                            <button type="submit" class="btn btn-sm btn-success">
                                                                {{  __('wording.edit') }}
                                                            </button>
                                                        </form>
                                                        <form id='delete-form' method="POST" action="{{ url()->current() . '/' . $audio->id }}">
                                                            @csrf
                                                            @method('delete')
                                                            <input type='hidden' name='brand' value='{{ $brand->id }}'>
                                                            <button type="submit" class="btn btn-sm btn-light btn-delete">
                                                                {{  __('wording.delete') }}
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @php $no++ @endphp
                                            @endforeach
                                        @else
                                            @include('layouts.table-empty', ['col' => 6])
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                            {{-- Pagination --}}
                            @if (isset($audios) && $audios->count() > 0)
                                <div class="float-right">
                                    {{ $audios->links() }}
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/general.js') }}"></script>
    @include('script.confirm-delete')
@endsection