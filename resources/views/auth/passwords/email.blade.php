@extends('layouts.master-auth', ['title' => $title])

@section('content')
    <div class='container pt-5'>
        <div class='row d-flex justify-content-center'>
            <div class='col-md-8'>
                @include('layouts.errors')
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form method='POST' action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group">
                        <label>{{ __('auth.email') }}:</label>
                        <input type="email" class="form-control" placeholder="{{ __('wording.input') }} {{ strtolower(__('auth.email')) }}" name="email" value="{{ old('email') }}" required autofocus>
                    </div>

                    <button type="submit" class="btn btn-success">{{ __('wording.submit') }}</button>
                    <a href='{{ url()->previous() }}' class='btn btn-outline-secondary'>{{ __('wording.back') }}</a>
                </form>
            </div>
        </div>
    </div>
@endsection