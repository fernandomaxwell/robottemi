@extends('layouts.master-auth', ['title' => $title])

@section('content')
    <div class='container pt-5'>
        <div class='row d-flex justify-content-center'>
            <div class='col-md-8'>
                @include('layouts.errors')
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form method='POST' action="{{ route('password.update') }}">
                    @csrf
                    <input type='hidden' name='token' value="{{ $token }}">

                    <div class="form-group">
                        <label>{{ __('auth.email') }}:</label>
                        <input type="email" name='email' class="form-control" value="{{ $email ?? old('email') }}" required readonly>
                    </div>

                    <div class="form-group">
                        <label>{{ __('auth.password') }}:</label>
                        <input type="password" name='password' class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>{{ __('auth.confirm_password') }}:</label>
                        <input type="password" name='password_confirmation' class="form-control"  required>
                    </div>

                    <button type="submit" class="btn btn-success">{{ __('wording.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection