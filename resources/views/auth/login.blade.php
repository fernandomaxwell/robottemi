@extends('layouts.master-auth', ['title' => $title])

@section('content')
    <div class='container py-5'>
        <div class='row d-flex justify-content-center'>
            <div class='col-md-8'>
                @include('layouts.errors')

                <form method='POST' action="{{ route('login') }}">
                    @csrf

                    <div class="form-group">
                        <label>{{ __('auth.email') }}:</label>
                        <input type="email" class="form-control" placeholder="{{ __('wording.input') }} {{ strtolower(__('auth.email')) }}" name="email" value="{{ old('email') }}" required autofocus>
                    </div>

                    <div class="form-group">
                        <label>{{ __('auth.password') }}:</label>
                        <input type="password" class="form-control" placeholder="{{ __('wording.input') }} {{ strtolower(__('auth.password')) }}" name="password" required autocomplete='off'>
                    </div>

                    <button type="submit" class="btn btn-success">{{ __('auth.login') }}</button>
                    <a href="{{ route('password.request') }}" class='text-dark'>{{ __('auth.forgot_password') }}?</a>
                </form>
            </div>
        </div>
    </div>
@endsection