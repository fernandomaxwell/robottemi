@extends('layouts.master-auth', ['title' => $title])

@section('content')
    <div class='container pt-5'>
        <div class='row d-flex justify-content-center'>
            <div class='col-md-8'>
                @include('layouts.errors')

                <form method='POST' action="{{ route('register') }}">
                    @csrf
                    <div class="form-group">
                        <label>*{{ __('wording.name') }}:</label>
                        <input type="text" class="form-control" placeholder="{{ __('wording.input') }} {{ strtolower(__('wording.name')) }}" name="name" value="{{ old('name') }}" maxlength='255' autofocus required>
                    </div>

                    <div class="form-group">
                        <label>*{{ __('auth.email') }}:</label>
                        <input type="email" class="form-control" placeholder="{{ __('wording.input') }} {{ strtolower(__('auth.email')) }}" name="email" value="{{ old('email') }}" maxlength='255' required>
                    </div>

                    <div class="form-group">
                        <label>*{{ __('auth.password') }}:</label>
                        <input type="password" class="form-control" placeholder="{{ __('wording.input') }} {{ strtolower(__('auth.password')) }}" name="password" required autocomplete="new-password">
                    </div>

                    <div class="form-group">
                        <label>*{{ __('auth.confirm_password') }}:</label>
                        <input type="password" class="form-control" placeholder="{{ __('wording.input') }} {{ strtolower(__('auth.confirm_password')) }}" name="password_confirmation" required autocomplete="new-password">
                    </div>

                    <p><small>* {{ __('wording.required') }}</small></p>

                    <button type="submit" class="btn btn-success">{{ __('wording.submit') }}</button>
                    <a href='{{ url()->previous() }}' class='btn btn-outline-secondary'>{{ __('wording.back') }}</a>
                </form>
            </div>
        </div>
    </div>
@endsection