@extends('layouts.master', ['title' => $title])

@section('css')
    @include('style.general')
@endsection

@section('content')
    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    {{-- Summary --}}
    <div class='row'>
        {{-- Total Transaction --}}
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                {{ __('wording.transaction') }}
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id='total-transaction'>-</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calculator fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Total Revenue --}}
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                {{ __('wording.revenue') }}
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id='total-revenue'>-</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calculator fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">{{ __('wording.list') }} {{ ucwords($title) }}</h6>
        </div>

        <div class="card-body">
            {{-- Filter --}}
            <div class='row'>
                <div class='col-lg-6'>
                    <form method='GET' action='{{ url()->current() }}' id='form-search'>
                        {{-- Keywords --}}
                        @include('layouts.keyword')
                    </form>
                </div>
            </div>

            {{-- Table --}}
            <div class='table-responsive'>
                <table class='table table-hover table-bordered'>
                    <thead>
                        <th>No.</th>
                        <th style='min-width: 200px;'>@sortablelink('client.name', trans('wording.client.name'))</th>
                        <th style='min-width: 200px;'>@sortablelink('client.email', trans('wording.client.email'))</th>
                        <th style='min-width: 150px;'>@sortablelink('created_at', trans('wording.purchase_date'))</th>
                        <th style='min-width: 150px;'>@sortablelink('total_price', trans('wording.value'))</th>
                        <th style='min-width: 150px;'>@sortablelink('status.name', trans('wording.status'))</th>
                        <th style='min-width: 150px;'>{{ __('wording.action') }}</th>
                    </thead>

                    <tbody>
                        @if(isset($orders) && $orders->count() > 0)
                            @php
                                $no = (($orders->currentpage() - 1) * $orders->perpage()) + 1;
                            @endphp
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $order->client->name }}</td>
                                    <td>{{ $order->client->email }}</td>
                                    <td>{{ date('Y-m-d', strtotime($order->created_at)) }}</td>
                                    <td>Rp {{ number_format($order->total_price, 0, ',', '.') }},-</td>
                                    <td>{{ $order->status->name }}</td>
                                    <td>
                                        <a href="{{ url()->current() . '/' . $order->id }}" class='btn btn-sm btn-primary' data-toggle="tooltip" title="{{  __('wording.view') }}">
                                            <i class='fa fa-eye'></i>
                                        </a>
                                    </td>
                                </tr>
                                @php $no++; @endphp
                            @endforeach
                        @else
                            @include('layouts.table-empty', ['col' => 7])
                        @endif
                    </tbody>
                </table>
            </div>

            {{-- Pagination --}}
            @if(isset($orders) && $orders->count() > 0)
                <div class="float-right">
                    {{ $orders->links() }}
                </div>
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/general.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        const countTransaction = (keywords = null) => new Promise((resolve, reject) => {
            $.ajax({
                type: 'POST',
                data: { keywords },
                url: "{{ url('ajax/total-transaction') }}",
                success: (response) => {
                    resolve(response.items.total);
                },
                error: (err) => {
                    reject(err);
                }
            });
        });

        const countRevenue = (keywords = null) => new Promise((resolve, reject) => {
            $.ajax({
                type: 'POST',
                data: { keywords },
                url: "{{ url('ajax/total-revenue') }}",
                success: (response) => {
                    resolve(response.items.total);
                },
                error: (err) => {
                    reject(err);
                }
            });
        });

        {{-- On Load --}}
        $(function () {
            (async () => {
                let result = await Promise.all([
                    countTransaction('{{ $request->keywords }}'), 
                    countRevenue('{{ $request->keywords }}'), 
                ]);

                {{-- Summary --}}
                $('#total-transaction').html(result[0]);
                $('#total-revenue').html(result[1]);
            })().catch(err => {
                console.log(err);
            });
        });
    </script>
@endsection