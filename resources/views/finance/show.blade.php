@extends('layouts.master', ['title' => $title])

@section('content')
    {{-- Page Heading --}}
    <div class='row pb-3'>
        <div class='col-md-6'>
            <a href='{{ url("finance") }}' class='btn btn-outline-secondary'>
                <i class='fa fa-arrow-circle-left'></i> {{ __('wording.back') }}
            </a>
        </div>
    </div>

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    <div class="card shadow mb-3">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">{{ __('wording.order.order_info') }}</h6>
        </div>

        <div class='card-body'>
            <div class='row'>
                <div class='col-lg-6'>
                    <div class='form-group'>
                        <label><b>{{ __('wording.client.name') }}:</b></label>
                        <p>{{ $order->client->name }}</p>
                    </div>

                    <div class='form-group'>
                        <label><b>{{ __('wording.client.email') }}:</b></label>
                        <p>{{ $order->client->email }}</p>
                    </div>
                </div>

                <div class='col-lg-6'>
                    <div class='form-group'>
                        <label><b>{{ __('wording.purchase_date') }}:</b></label>
                        <p>{{ date('Y-m-d', strtotime($order->created_at)) }}</p>
                    </div>

                    <div class='form-group'>
                        <label><b>{{ __('wording.value') }}:</b></label>
                        <p>Rp{{ number_format($order->total_price, 0, ',', '.') }},-</p>
                    </div>

                    <div class='form-group'>
                        <label><b>{{ __('wording.status') }}:</b></label>
                        <p>{{ $order->status->name }}</p>
                    </div>
                </div>

                <div class='col-lg-12'>
                    <div class='form-group'>
                        <label><b>{{ __('wording.order.detail') }}:</b></label>
                        <div class='table-responsive'>
                            <table class='table table-hover table-bordered'>
                                <thead>
                                    <th style='min-width: 200px;'>{{ __('wording.package') }}</th>
                                    <th style='min-width: 200px;'>{{ __('wording.quantity') }}</th>
                                    <th style='min-width: 150px;'>{{ __('wording.price') }}</th>
                                    <th style='min-width: 150px;'>{{ __('wording.value') }}</th>
                                </thead>

                                <tbody>
                                    @foreach($order->details as $detail)
                                        <tr>
                                            <td>{{ $detail->package->name }}</td>
                                            <td>{{ $detail->quantity }}</td>
                                            <td>Rp {{ number_format($detail->package->price, 0, ',', '.') }},-</td>
                                            <td>Rp {{ number_format($detail->quantity * $detail->package->price, 0, ',', '.') }},-</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection