@extends('layouts.master', ['title' => $title])

@section('css')
    @include('style.general')
@endsection

@section('content')
    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">{{ __('wording.list') }} {{ ucwords($title) }}</h6>
        </div>

        <div class="card-body">
            {{-- Filter --}}
            <div class='row'>
                <div class='col-lg-6'>
                    <form method='GET' action='{{ url()->current() }}' id='form-search'>
                        {{-- Keywords --}}
                        @include('layouts.keyword')
                    </form>
                </div>
            </div>

            {{-- Table --}}
            <div class='table-responsive'>
                <table class='table table-hover table-bordered'>
                    <thead>
                        <th>No.</th>
                        <th style='min-width: 200px;'>@sortablelink('name', trans('wording.name'))</th>
                        <th style='min-width: 200px;'>@sortablelink('email', trans('auth.email'))</th>
                        <th style='min-width: 200px;'>@sortablelink('company', trans('wording.company'))</th>
                        <th style='min-width: 150px;'>@sortablelink('whatsapp_number', trans('wording.lead_generation.whatsapp'))</th>
                        {{-- <th style='min-width: 150px;'>{{ __('wording.action') }}</th> --}}
                    </thead>

                    <tbody>
                        @if(isset($lead_generations) && $lead_generations->count() > 0)
                            @php
                                $no = (($lead_generations->currentpage() - 1) * $lead_generations->perpage()) + 1;
                            @endphp
                            @foreach($lead_generations as $lead_generation)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $lead_generation->name }}</td>
                                    <td>{{ $lead_generation->email }}</td>
                                    <td>{{ $lead_generation->company }}</td>
                                    <td>{{ '+62' . $lead_generation->whatsapp_number }}</td>
                                    {{-- <td>
                                        <a href="{{ url()->current() . '/' . $lead_generation->id . '/edit' }}" class='btn btn-sm btn-success' data-toggle="tooltip" title="{{  __('wording.edit') }}">
                                            <i class='fa fa-edit'></i>
                                        </a>
                                    </td> --}}
                                </tr>
                                @php $no++ @endphp
                            @endforeach
                        @else
                            @include('layouts.table-empty', ['col' => 5])
                        @endif
                    </tbody>
                </table>
            </div>

            {{-- Pagination --}}
            @if(isset($lead_generations) && $lead_generations->count() > 0)
                <div class="float-right">
                    {{ $lead_generations->links() }}
                </div>
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/general.js') }}"></script>
@endsection