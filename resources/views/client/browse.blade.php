@extends('layouts.master', ['title' => $title])

@section('css')
    @include('style.general')
@endsection

@section('content')
    {{-- Page Heading --}}
    {{-- @include('layouts.button-add') --}}

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">{{ __('wording.list') }} {{ ucwords($title) }}</h6>
        </div>

        <div class="card-body">
            {{-- Filter --}}
            <div class='row'>
                <div class='col-lg-6'>
                    <form method='GET' action='{{ url()->current() }}' id='form-search'>
                        {{-- Keywords --}}
                        @include('layouts.keyword')
                    </form>
                </div>
            </div>

            {{-- Table --}}
            <div class='table-responsive'>
                <table class='table table-hover table-bordered'>
                    <thead>
                        <th>No.</th>
                        <th style='min-width: 200px;'>@sortablelink('name', trans('wording.client.name'))</th>
                        <th style='min-width: 200px;'>@sortablelink('email', trans('wording.client.email'))</th>
                        <th style='min-width: 170px;'>@sortablelink('company', trans('wording.client.company'))</th>
                        <th style='min-width: 170px;'>{{ __('wording.package') }}</th>
                        <th style='min-width: 150px;'>{{ __('wording.action') }}</th>
                    </thead>

                    <tbody>
                        @if(isset($clients) && $clients->count() > 0)
                            @php
                                $no = (($clients->currentpage() - 1) * $clients->perpage()) + 1;
                            @endphp
                            @foreach($clients as $client)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $client->name }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td>{{ $client->company }}</td>
                                    <td>
                                        @if(!$client->packages->isEmpty())
                                            <ul>
                                                @foreach($client->packages as $package)
                                                    <li>{{ $package->name }}</li>
                                                @endforeach
                                            </ul>
                                        @else - @endif
                                    </td>
                                    <td>
                                        <a href="{{ url()->current() . '/' . $client->id }}" class='btn btn-sm btn-primary' data-toggle="tooltip" title="{{  __('wording.view') }}">
                                            <i class='fa fa-eye'></i>
                                        </a>
                                        <form id='delete-form' method="POST" action="{{ url()->current() . '/' . $client->id }}">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip" title="{{  __('wording.delete') }}">
                                                <i class='fa fa-trash'></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @php $no++ @endphp
                            @endforeach
                        @else
                            @include('layouts.table-empty', ['col' => 6])
                        @endif
                    </tbody>
                </table>
            </div>

            {{-- Pagination --}}
            @if (isset($clients) && $clients->count() > 0)
                <div class="float-right">
                    {{ $clients->links() }}
                </div>
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/general.js') }}"></script>
    @include('script.confirm-delete')
@endsection
