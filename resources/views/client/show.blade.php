@extends('layouts.master', ['title' => $title])

@section('content')
    {{-- Page Heading --}}
    <div class='row pb-3'>
        <div class='col-md-6'>
            <a href='{{ url("client") }}' class='btn btn-outline-secondary'>
                <i class='fa fa-arrow-circle-left'></i> {{ __('wording.back') }}
            </a>
        </div>
    </div>

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    <div class='row'>
        <div class='col-lg-6'>
            <div class="card shadow mb-3">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold">{{ __('wording.client.personal_info') }}</h6>
                </div>
            
                <div class='card-body'>
                    <div class='form-group'>
                        <label><b>{{ __('wording.client.name') }}:</b></label>
                        <p>{{ $client->name }}</p>
                    </div>

                    <div class='form-group'>
                        <label><b>{{ __('wording.client.email') }}:</b></label>
                        <p>{{ $client->email }}</p>
                    </div>

                    <div class='form-group'>
                        <label><b>{{ __('wording.client.company') }}:</b></label>
                        <p>{{ $client->company }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class='col-lg-6'>
            <div class="card shadow mb-3">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold">{{ __('wording.package') }}</h6>
                </div>

                <div class='card-body'>
                    <div class='table-responsive'>
                        <table class='table table-hover table-bordered'>
                            <thead>
                                <th style='min-width: 120px;'>{{ __('wording.name') }}</th>
                                <th style='min-width: 120px;'>{{ __('wording.purchase_date') }}</th>
                                <th style='min-width: 120px;'>{{ __('wording.expired_date') }}</th>
                            </thead>

                            <tbody>
                                @if(!$client->packages->isEmpty())
                                    @foreach($client->packages as $package)
                                        <tr>
                                            <td>{{ $package->name }}</th>
                                            <td>{{ date('Y-m-d', strtotime($package->pivot->created_at)) }}</td>
                                            <td>{{ date('Y-m-d', strtotime($package->pivot->expired_at)) }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    @include('layouts.table-empty', ['col' => 4])
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection