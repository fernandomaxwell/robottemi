@extends('layouts.master', ['title' => $title])

@section('css')
    @include('style.general')
@endsection

@section('content')
    {{-- Page Heading --}}
    @include('layouts.button-add')

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">{{ __('wording.list') }} {{ ucwords($title) }}</h6>
        </div> --}}

        <div class="card-body">
            {{-- Filter --}}
            <div class='row'>
                <div class='col-lg-6'>
                    <form method='GET' action='{{ url()->current() }}' id='form-search'>
                        {{-- Keywords --}}
                        @include('layouts.keyword')
                    </form>
                </div>
            </div>

            {{-- Table --}}
            <div class='table-responsive'>
                <table class='table table-hover table-bordered'>
                    <thead>
                        <th>No.</th>
                        <th style='min-width: 150px;'>{{ __('wording.brand.name') }}</th>
                        <th style='min-width: 150px;'>{{ __('auth.username') }}</th>
                        <th style='min-width: 150px;'>{{ __('wording.brand.category') }}</th>
                        <th style='min-width: 150px;'>{{ __('wording.action') }}</th>
                    </thead>

                    <tbody>
                        @if(isset($brands) && $brands->count() > 0)
                            @php
                                $no = (($brands->currentpage() - 1) * $brands->perpage()) + 1;
                            @endphp
                            @foreach($brands as $brand)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $brand->name }}</td>
                                    <td>{{ $brand->username }}</td>
                                    <td>{{ $brand->type }}</td>
                                    <td>
                                        <a href="{{ url()->current() . '/' . $brand->id . '/edit' }}" class='btn btn-sm btn-secondary'>
                                            {{  __('wording.edit') }}
                                        </a>
                                        <form method="GET" action="{{  url('advertisement') }}">
                                            <input type='hidden' name='brand' value='{{ $brand->id }}'>
                                            <button type="submit" class="btn btn-sm btn-success">
                                                {{  __('wording.video') }}
                                            </button>
                                        </form>
                                        <form method="GET" action="{{  url('audio') }}">
                                            <input type='hidden' name='brand' value='{{ $brand->id }}'>
                                            <button type="submit" class="btn btn-sm btn-success">
                                                {{  __('wording.audio') }}
                                            </button>
                                        </form>
                                        <form id='delete-form' method="POST" action="{{ url()->current() . '/' . $brand->id }}">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-sm btn-delete">
                                                {{  __('wording.delete') }}
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @php $no++; @endphp
                            @endforeach
                        @else
                            @include('layouts.table-empty', ['col' => 5])
                        @endif
                    </tbody>
                </table>
            </div>

            {{-- Pagination --}}
            @if (isset($brands) && $brands->count() > 0)
                <div class="float-right">
                    {{ $brands->links() }}
                </div>
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/general.js') }}"></script>
    @include('script.confirm-delete')
@endsection