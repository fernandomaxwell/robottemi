<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ ucwords($title) }} - {{ config('app.name') }}</title>

        {{-- Custom fonts --}}
        <link href="{{ asset('css/all.min.css') }}" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        {{-- Custom styles --}}
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom-style.css') }}?v={{ request()->get('asset_version') }}" rel="stylesheet">

        {{-- Icon --}}
        <link href="{{ asset(request()->get('logo_dark')) }}?v={{ request()->get('asset_version') }}" rel="icon">

        @yield('css')
    <head>

    <body id="page-top">
        {{-- Page Wrapper --}}
        <div id="wrapper">
            @include('layouts.sidebar')

            {{-- Content Wrapper --}}
            <div id="content-wrapper" class="d-flex flex-column">

                {{-- Main Content --}}
                <div id="content">
                    @include('layouts.topbar')

                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>

                {{-- Footer --}}
                @include('layouts.footer')
            <div>

        </div>

        {{-- Scroll to Top Button --}}
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        {{-- Bootstrap core JavaScript --}}
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

        {{-- Core plugin JavaScript --}}
        <script src="{{ asset('js/jquery.easing.min.js') }}"></script>

        {{-- Custom scripts for all pages --}}
        <script src="{{ asset('js/custom.js') }}?v={{ request()->get('asset_version') }}"></script>

        @yield('javascript')
    </body>
</html>