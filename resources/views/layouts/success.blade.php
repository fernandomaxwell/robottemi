@if (session('success'))
    <div class="modal alert-modal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content bg-alert">
                <div class="modal-body">
                    <div class='row'>
                        <div class='col-md-12'>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class='col-md-12 text-center'>
                            <img src="{{ asset('img/alert-background.png') }}">
                            <h5 class='mt-2 font-weight-bold'>{{ session('success') }}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
