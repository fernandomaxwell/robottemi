<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    {{-- Toggler (topbar) --}}
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    {{-- Navbar --}}
    <ul class="navbar-nav">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle active" href="#" data-toggle="dropdown">
                <span class="mr-2 d-lg-inline text-gray-600 small">
                    {{ App::getLocale() }}
                </span>
            </a>
            <div class="dropdown-menu">
                @foreach(request()->get('languages') as $language)
                    <a class="dropdown-item" href="{{ url('/language/' . $language->code) }}">
                        {{ $language->code }}
                    </a>
                @endforeach
            </div>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto">
        <div class="topbar-divider d-none d-sm-block"></div>

        {{-- Nav Item --}}
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if(Auth::user()->image_path)
                    <img class="img-profile rounded-circle" src="{{ asset('storage/' . Auth::user()->image_path) }}" alt='photo'>
                @else
                    <img class="img-profile rounded-circle" src="{{ asset('img/user.png') }}">
                @endif
                <span class="ml-2 d-none d-lg-inline text-gray-600 small">{{ strtok(Auth::user()->name, ' ') }}</span>
            </a>

            {{-- Dropdown --}}
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{ url('profile-management') }}">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    {{ __('wording.profile_management') }}
                </a>
                <div class="dropdown-divider"></div>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button class="dropdown-item" type="submit">
                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i> {{ __('auth.logout') }}
                    </button>
                </form>
            </div>
        </li>
    </ul>
</nav>