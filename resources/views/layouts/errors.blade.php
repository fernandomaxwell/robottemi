@if ($errors->any())
    <div class="modal alert-modal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content bg-alert">
                <div class="modal-body">
                    <div class='row'>
                        <div class='col-md-12'>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class='col-md-12 text-center'>
                            <img src="{{ asset('img/alert-background.png') }}">
                            <ul class='mt-2' style='list-style-type: none;'>
                                @foreach ($errors->all() as $error)
                                    <li>
                                        <h5 class='font-weight-bold text-danger'>{{ $error }}</h5>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif