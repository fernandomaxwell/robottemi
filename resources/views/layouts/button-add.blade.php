<div class='row pb-3'>
    <div class='col-md-6'>
        @if (isset($brand))
            <form method="GET" action="{{ url()->current() . '/create' }}">
                <input type='hidden' name='brand' value='{{ $brand->id }}'>
                <button type="submit" class="btn btn-success">
                    {{ strtoupper(__('wording.add')) }} <i><img src="{{ asset('img/icon/add-square.png') }}"></i>
                </button>
            </form>
        @else
            <a href="{{ url()->current() . '/create' }}" class='btn btn-success'>
                {{ strtoupper(__('wording.add')) }} <i><img src="{{ asset('img/icon/add-square.png') }}"></i>
            </a>
        @endif
    </div>
</div>