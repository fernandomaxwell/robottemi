<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ ucwords($title) }} - {{ config('app.name') }}</title>

        {{-- Custom fonts --}}
        <link href="{{ asset('css/all.min.css') }}" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        {{-- Custom styles --}}
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom-style.css') }}?v={{ request()->get('asset_version') }}" rel="stylesheet">
        <link href="{{ asset('css/auth.css') }}?v={{ request()->get('asset_version') }}" rel="stylesheet">

        {{-- Icon --}}
        <link href="{{ asset(request()->get('logo_dark')) }}?v={{ request()->get('asset_version') }}" rel="icon">

        @yield('css')
    <head>

    <body>
        <div class='row' style='width: 100%;'>
            <div class='col-md-6 d-none d-md-block'>
                <div id="login-background" class="carousel slide" data-ride="carousel">
                    <ul class="carousel-indicators">
                        @foreach(config('app.login_backgrounds') as $key => $background)
                            <li data-target="#login-background" data-slide-to="{{ $key }}" @if($key == 0) class="active" @endif></li>
                        @endforeach
                    </ul>

                    <div class="carousel-inner">
                        @foreach(config('app.login_backgrounds') as $key => $background)
                            <div class="carousel-item @if($key == 0) active @endif" style="background-image: url({{ asset($background) }}?v={{ request()->get('asset_version') }});">
                                <img src="{{ asset($background) }}?v={{ request()->get('asset_version') }}">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class='col-md-6'>
                <nav class="navbar navbar-expand-sm">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-dark active" href="#" data-toggle="dropdown">
                                {{ App::getLocale() }}
                            </a>
                            <div class="dropdown-menu">
                                @foreach(request()->get('languages') as $language)
                                    <a class="dropdown-item" href="{{ url('/language/' . $language->code) }}">
                                        {{ $language->code }}
                                    </a>
                                @endforeach
                            </div>
                        </li>
                    </ul>
                </nav>

                <div class='container text-center mt-5 pt-5'>
                    <img src="{{ asset(request()->get('logo_dark')) }}?v={{ request()->get('asset_version') }}">
                </div>

                {{-- Main Content --}}
                @yield('content')
            </div>
        </div>

        {{-- Bootstrap core JavaScript --}}
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

        {{-- Core plugin JavaScript --}}
        <script src="{{ asset('js/jquery.easing.min.js') }}"></script>

        {{-- Custom scripts for all pages --}}
        <script src="{{ asset('js/custom.js') }}?v={{ request()->get('asset_version') }}"></script>

        @yield('javascript')
    </body>
</html>