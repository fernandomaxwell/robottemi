<div class='row'>
    @if (isset($brand))
        <input type='hidden' name='brand' value='{{ $brand->id }}'>
    @endif
    @if (isset($request->type))
        <input type='hidden' name='type' value='{{ $request->type }}'>
    @endif
    <div class='col-lg-6 form-group'>
        <input type='text' class='form-control' name='keywords' maxlength='255' placeholder="{{ __('wording.search') }}" value="{{ $request->keywords }}">
    </div>
    <div class='col-lg-3 form-group'>
        <button type='submit' class='btn btn-success'>
            <i class='fa fa-search'></i>
        </button>
    </div>
</div>