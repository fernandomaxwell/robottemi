<ul class="navbar-nav bg-sidebar sidebar sidebar-dark accordion" id="accordionSidebar">
    {{-- Logo --}}
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url(request()->get('default_redirect_to')) }}">
        <img src="{{ asset(request()->get('logo_white')) }}?v={{ request()->get('asset_version') }}" class='mx-2' style="width:50px;">
    </a>

    {{-- Divider --}}
    <hr class="sidebar-divider my-0">

    {{-- Nav Item --}}
    @if(request()->get('menus') !== null && !request()->get('menus')->isEmpty())
        @foreach(request()->get('menus') as $menu)
            <li class="nav-item {{ (request()->is($menu->link . '*')) ? 'active' : '' }}">
                @if($menu->children->isEmpty())
                    <a class="nav-link" href="{{ url($menu->link) }}">
                        <i><img src="{{ asset($menu->icon) }}?v={{ request()->get('asset_version') }}" style='width: 20px'></i>
                        <span>{{ $menu->name }}</span>
                    </a>
                @else
                    <a class="nav-link collapsed" href="{{ url($menu->link) }}" data-toggle="collapse" data-target="#collapse_{{ $menu->id }}" aria-expanded="true" aria-controls="collapse_{{ $menu->id }}">
                        <i><img src="{{ asset($menu->icon) }}?v={{ request()->get('asset_version') }}" style='width: 20px'></i>
                        <span>{{ $menu->name }}</span>
                    </a>
                    <div id="collapse_{{ $menu->id }}" class="collapse" aria-labelledby="heading_{{ $menu->id }}" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            @foreach($menu->children as $submenu)
                                <a class="collapse-item {{ (request()->is($submenu->link)) ? 'active' : '' }}" href="{{ url($submenu->link) }}">{{ $submenu->name }}</a>
                            @endforeach
                        </div>
                    </div>
                @endif
            </li>
        @endforeach
    @endif

    {{-- Divider --}}
    <hr class="sidebar-divider">

    {{-- Toggler (sidebar) --}}
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>