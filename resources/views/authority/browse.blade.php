@extends('layouts.master', ['title' => $title])

@section('css')
    @include('style.general')
@endsection

@section('content')
    {{-- Page Heading --}}
    @include('layouts.button-add')

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">{{ __('wording.list') }} {{ ucwords($title) }}</h6>
        </div> --}}

        <div class="card-body">
            {{-- Filter --}}
            <div class='row'>
                <div class='col-lg-6'>
                    <form method='GET' action='{{ url()->current() }}' id='form-search'>
                        {{-- Keywords --}}
                        @include('layouts.keyword')
                    </form>
                </div>
            </div>

            {{-- Table --}}
            <div class='table-responsive bg-table rounded'>
                <table class='table table-hover table-bordered'>
                    <thead>
                        <th>No.</th>
                        <th style='min-width: 175px;'>{{ __('wording.name') }}</th>
                        <th style='min-width: 175px;'>{{ __('auth.email') }}</th>
                        <th style='min-width: 150px;'>{{ __('wording.role') }}</th>
                        <th style='min-width: 150px;'>{{ __('wording.action') }}</th>
                    </thead>

                    <tbody>
                        @if(isset($users) && $users->count() > 0)
                            @php
                                $no = (($users->currentpage() - 1) * $users->perpage()) + 1;
                            @endphp
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role->name }}</td>
                                    <td>
                                        <a href="{{ url()->current() . '/' . $user->id . '/edit' }}" class='btn btn-sm btn-secondary'>
                                            {{  __('wording.edit') }}
                                        </a>
                                        <form id='delete-form' method="POST" action="{{ url()->current() . '/' . $user->id }}">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-sm btn-delete">
                                                {{  __('wording.delete') }}
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @php $no++; @endphp
                            @endforeach
                        @else
                            @include('layouts.table-empty', ['col' => 5])
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/general.js') }}"></script>
    @include('script.confirm-delete')
@endsection