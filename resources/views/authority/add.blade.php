@extends('layouts.master', ['title' => $title])

@section('css')
    @include('style.general')
@endsection

@section('content')
    {{-- Page Heading --}}
    <div class='row pb-3'>
        <div class='col-md-6'>
            <a href='{{ url("authority") }}' class='btn btn-outline-secondary'>
                <i class='fa fa-arrow-circle-left'></i> {{ __('wording.back') }}
            </a>
        </div>
    </div>

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    <div class='row bg-brand-add rounded'>
        <div class='col-md-6 mx-4 my-5'>
            {{-- Form --}}
            <form method='POST' action="{{ url('authority') }}" enctype="multipart/form-data">
                @csrf

                <div class="card shadow mb-3">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold">{{ __('wording.form.authority') }}</h6>
                    </div>

                    <div class='card-body'>
                        <div class='row'>
                            <div class='col-md-12'>
                                <div class="form-group">
                                    <label>{{ __('wording.name') }}:</label>
                                    <input type="text" class="form-control" name='name' value="{{ old('name') }}" maxlength='255' required autofocus>
                                </div>

                                <div class="form-group">
                                    <label>{{ __('auth.email') }}:</label>
                                    <input type="email" class="form-control" name='email' value="{{ old('email') }}" maxlength='255' required>
                                </div>

                                <div class="form-group">
                                    <label>{{ __('auth.password') }}</label>
                                    <input type="password" class="form-control" id="password" name='password' required>
                                    <input type='checkbox' id='show_password'> {{ __('wording.show') }}
                                </div>

                                <div class="form-group">
                                    <label>{{ __('auth.confirm_password') }}</label>
                                    <input type="password" class="form-control" id="password_confirmation" name='password_confirmation' required>
                                    <input type='checkbox' id='show_password_confirmation'> {{ __('wording.show') }}
                                </div>

                                <div class='form-group'>
                                    <label>{{ __('wording.role') }}:</label>
                                    <select class='form-control' name='role' required>
                                        @foreach($roles as $role)
                                            <option value='{{ $role->id }}' @if(old('role') == $role->id) selected @endif>
                                                {{ $role->name }}
                                            </audio>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success mb-4">{{ __('wording.submit') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $('#show_password').on('change', function () {
            if ($('#password').attr('type') == 'password') {
                $('#password').prop('type', 'text');
            } else {
                $('#password').prop('type', 'password');
            }
        });

        $('#show_password_confirmation').on('change', function () {
            if ($('#password_confirmation').attr('type') == 'password') {
                $('#password_confirmation').prop('type', 'text');
            } else {
                $('#password_confirmation').prop('type', 'password');
            }
        });

        $('#show_old_password').on('change', function () {
            if ($('#old_password').attr('type') == 'password') {
                $('#old_password').prop('type', 'text');
            } else {
                $('#old_password').prop('type', 'password');
            }
        });
    </script>
@endsection