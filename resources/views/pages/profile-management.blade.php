@extends('layouts.master', ['title' => $title])

@section('content')
    {{-- Page Heading --}}
    <div class='row pb-3'>
        <div class='col-md-6'>
            <a href='{{ url()->previous() }}' class='btn btn-outline-secondary'>
                <i class='fa fa-arrow-circle-left'></i> {{ __('wording.back') }}
            </a>
        </div>
    </div>

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    {{-- Form --}}
    <div class='row'>        
        <div class='col-lg-6'>
            <div class="card shadow mb-3">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold">{{ __('wording.edit') . ' ' . __('auth.password') }}</h6>
                </div>

                <div class='card-body'>
                    <form method='POST' action="{{ url()->current() . '/change-password' }}">
                        @csrf

                        <div class="form-group">
                            <label>{{ __('auth.old_password') }}</label>
                            <input type="password" class="form-control" id="old_password" name='old_password' required>
                            <input type='checkbox' id='show_old_password'> {{ __('wording.show') }}
                        </div><hr>

                        <div class="form-group">
                            <label>{{ __('auth.password') }}</label>
                            <input type="password" class="form-control" id="password" name='password' required>
                            <input type='checkbox' id='show_password'> {{ __('wording.show') }}
                        </div>

                        <div class="form-group">
                            <label>{{ __('auth.confirm_password') }}</label>
                            <input type="password" class="form-control" id="password_confirmation" name='password_confirmation' required>
                            <input type='checkbox' id='show_password_confirmation'> {{ __('wording.show') }}
                        </div>

                        <button type='submit' class='btn btn-success mb-4'>{{ __('wording.submit') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $('#show_password').on('change', function () {
            if ($('#password').attr('type') == 'password') {
                $('#password').prop('type', 'text');
            } else {
                $('#password').prop('type', 'password');
            }
        });

        $('#show_password_confirmation').on('change', function () {
            if ($('#password_confirmation').attr('type') == 'password') {
                $('#password_confirmation').prop('type', 'text');
            } else {
                $('#password_confirmation').prop('type', 'password');
            }
        });

        $('#show_old_password').on('change', function () {
            if ($('#old_password').attr('type') == 'password') {
                $('#old_password').prop('type', 'text');
            } else {
                $('#old_password').prop('type', 'password');
            }
        });
    </script>
@endsection