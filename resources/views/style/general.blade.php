<style>
    td form {
        display: inline-block;
    }

    td a {
        margin-right: 5px;
    }

    .modal-dialog {
        overflow-y: initial !important
    }

    .modal-body {
        height: 75vh;
        overflow-y: auto;
    }

    .bg-brand-add,
    .bg-brand-edit {
        background-position: right center;
        background-repeat: no-repeat;
        filter: opacity(0.86);
    }

    .bg-brand-add {
        background-image: url("{{ asset('img/brand-add-background.png') }}"), linear-gradient(#1cc88a, #24d1a9);
    }

    .bg-brand-edit {
        background-image: url("{{ asset('img/brand-edit-background.png') }}"), linear-gradient(#1cc88a, #24d1a9);
    }

    .bg-general {
        background-color: rgb(133, 135, 150, .6);
        background-image: url("{{ asset('img/general-background.png') }}");
        background-position: right center;
        background-repeat: no-repeat;
        color: #fff
    }
</style>
