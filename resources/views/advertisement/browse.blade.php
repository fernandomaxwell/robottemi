@extends('layouts.master', ['title' => $title])

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    @include('style.general')
@endsection

@section('content')
    {{-- Page Heading --}}
    @include('layouts.button-add')

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">{{ __('wording.list') }} {{ ucwords($title) }}</h6>
        </div> --}}

        <div class="card-body rounded bg-video">
            {{-- Brand Name --}}
            <div class='mb-4'>
                <span class='h5'>{{ strtoupper($brand->name ? $brand->name : $brand->username) }} |</span>
                <span><small>{{ strtolower(__('wording.video')) }}</small></span>
            </div>

            <div class='rounded'>
                <ul class="nav nav-tabs">
                    @foreach ($audio_types as $audio_type)
                        <li class="nav-item">
                            <a class="nav-link @if($audio_type->name == $request->type) active @endif" href="{{ url()->current() . '?brand=' . $brand->id . '&type=' . $audio_type->name }}">
                                {{ $audio_type->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content bg-table">
                    @foreach ($audio_types as $audio_type)
                        <div class="tab-pane container py-4 @if($audio_type->name == $request->type) active @else hide @endif">
                            {{-- Filter --}}
                            <div class='row'>
                                <div class='col-lg-6'>
                                    <form method='GET' action='{{ url()->current() }}' id='form-search'>
                                        {{-- Keywords --}}
                                        @include('layouts.keyword')
                                    </form>
                                </div>

                                <div class='col-lg-6 text-right'>
                                    <label>{{ __('wording.ads.play_video_random') }}: </label>
                                    <input id='toggle-random' type="checkbox" @if($brand->random_ads) checked @endif data-toggle="toggle" data-onstyle="success">
                                </div>
                            </div>

                            {{-- Table --}}
                            <div class='table-responsive bg-table rounded'>
                                <table class='table table-hover table-bordered'>
                                    <thead>
                                        <th>No.</th>
                                        <th style='min-width: 200px;'>{{ __('wording.ads.name') }}</th>
                                        <th style='min-width: 150px;'>{{ __('wording.ads.advertiser') }}</th>
                                        <th style='width: 200px;'>{{ __('wording.ads.video') }}</th>
                                        <th style='min-width: 150px;'>{{ __('wording.ads.company_type') }}</th>
                                        <th style='min-width: 150px;'>{{ __('wording.action') }}</th>
                                    </thead>

                                    <tbody>
                                        @if(isset($advertisements) && $advertisements->count() > 0)
                                            @php
                                                $no = (($advertisements->currentpage() - 1) * $advertisements->perpage()) + 1;
                                            @endphp
                                            @foreach($advertisements as $advertisement)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $advertisement->name }}</td>
                                                    <td>{{ $advertisement->advertiser }}</td>
                                                    <td>
                                                        @if($advertisement->path)
                                                            @if (strpos($advertisement->path, 'http') !== false)
                                                                <video width='200' controls>
                                                                    <source src="{{ $advertisement->path }}" type="video/mp4">
                                                                </video>
                                                            @else
                                                                <video width='200' controls>
                                                                    <source src="{{ asset('storage/' . $advertisement->path) }}" type="video/mp4">
                                                                </video>
                                                            @endif
                                                        @else - @endif
                                                    </td>
                                                    <td>{{ $advertisement->type_of_advertiser_company }}</td>
                                                    <td>
                                                        <form method="GET" action="{{ url()->current() . '/' . $advertisement->id . '/edit' }}">
                                                            <input type='hidden' name='brand' value='{{ $brand->id }}'>
                                                            <button type="submit" class="btn btn-sm btn-success">
                                                                {{  __('wording.edit') }}
                                                            </button>
                                                        </form>
                                                        <form id='delete-form' method="POST" action="{{ url()->current() . '/' . $advertisement->id }}">
                                                            @csrf
                                                            @method('delete')
                                                            <input type='hidden' name='brand' value='{{ $brand->id }}'>
                                                            <button type="submit" class="btn btn-sm btn-light btn-delete">
                                                                {{  __('wording.delete') }}
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @php $no++ @endphp
                                            @endforeach
                                        @else
                                            @include('layouts.table-empty', ['col' => 6])
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                            {{-- Pagination --}}
                            @if (isset($advertisements) && $advertisements->count() > 0)
                                <div class="float-right">
                                    {{ $advertisements->links() }}
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('js/general.js') }}"></script>
    @include('script.confirm-delete')
    
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#toggle-random').change(function() {
            $.ajax({
                type: 'POST',
                data: { 
                    random: $(this).prop('checked'),
                    brand_id: {{ $brand->id }}
                },
                url: "{{ url('ajax/set-random-video') }}",
                success: (response) => {
                    // do nothing
                },
                error: (err) => {
                    alert(err.responseJSON.message)
                    location.reload()
                }
            })
        })
    </script>
@endsection