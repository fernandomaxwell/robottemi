@extends('layouts.master', ['title' => $title])

@section('css')
    @include('style.general')
@endsection

@section('content')
    {{-- Page Heading --}}
    <div class='row pb-3'>
        <div class='col-md-6'>
            <a href='{{ url("advertisement?brand=" . $brand->id) }}' class='btn btn-outline-secondary'>
                <i class='fa fa-arrow-circle-left'></i> {{ __('wording.back') }}
            </a>
        </div>
    </div>

    {{-- Error Message Handler --}}
    @include('layouts.errors')

    {{-- Success Message Handler --}}
    @include('layouts.success')

    {{-- Form --}}
    <form method='POST' action="{{ url('advertisement') }}" enctype="multipart/form-data">
        @csrf
        <input type='hidden' name='brand' value='{{ $brand->id }}'>

        <div class="card shadow mb-3">
            {{-- <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">{{ __('wording.form.advertisement') }}</h6>
            </div> --}}

            <div class='card-body rounded bg-general'>
                {{-- Brand Name --}}
                <div class='mb-4'>
                    <span class='h5'>{{ strtoupper($brand->name ? $brand->name : $brand->username) }} |</span>
                    <span><small>{{ strtolower(__('wording.video')) }}</small></span>
                </div>

                <div class='row'>
                    <div class='col-md-6'>
                        <div class="form-group">
                            <label>{{ __('wording.ads.name') }}:</label>
                            <input type="text" class="form-control" name='name' value="{{ old('name') }}" maxlength='255' autofocus>
                        </div>

                        <div class="form-group">
                            <label>{{ __('wording.ads.advertiser') }}:</label>
                            <input type="text" class="form-control" name='advertiser' value="{{ old('advertiser') }}" maxlength='255' autofocus>
                        </div>

                        <div class="form-group">
                            <label>{{ __('wording.ads.company_type') }}:</label>
                            <input type="text" class="form-control" name='type_of_advertiser_company' value="{{ old('type_of_advertiser_company') }}" maxlength='255' autofocus>
                        </div>

                        <div class='form-group'>
                            <label>{{ __('wording.ads.video') }}:</label>
                            <input type='file' class='form-control-file' accept="video/mp4" name='video' id='video' required>
                            <div class='my-2'>
                                <video id='video-preview' height='150' controls>
                                    <source type="video/mp4">
                                <video>
                            </div>
                        </div>

                        <div class='form-group'>
                            <label>{{ __('wording.type') }}:</label>
                            <select class='form-control' name='type' required>
                                @foreach($audio_types as $audio_type)
                                    <option value='{{ $audio_type->id }}' @if(old('type') == $audio_type->id) selected @endif>
                                        {{ $audio_type->name }}
                                    </audio>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>{{ __('wording.priority') }}:</label>
                            <select class='form-control' name='priority'>
                                <option value='' selected disabled>{{ __('wording.choose') }}</option>
                                @for($priority = 1; $priority <= $total_priority; $priority++)
                                    <option value='{{ $priority }}' @if($priority == old('priority')) selected @endif>{{ $priority }}</version>
                                @endfor
                            </select>
                        </div>
                        
                        <button type="submit" class="btn btn-success mb-4">{{ __('wording.submit') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('javascript')
    <script src="{{ asset('js/video-preview.js') }}"></script>
@endsection