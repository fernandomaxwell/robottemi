<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('language/{code}', 'SettingController@changeLanguage');
Route::group([
    'middleware' => [
        'getSetting',
    ],
], function () {
    Auth::routes(['register' => false]);
    Route::group([
        'middleware' => [
            'auth',
            'checkUserMenu',
            'checkAuthorize',
        ],
    ], function () {
        Route::group(['prefix' => 'ajax'], function () {
            // Finance
            // Route::post('total-transaction', 'AjaxController@countTransaction');
            // Route::post('total-revenue', 'AjaxController@countRevenue');

            Route::post('set-random-video', 'AjaxController@setRandomVideo');
        });
        Route::get('/', 'PageController@index');
        Route::resource('brand', 'BrandController');
        Route::resource('audio', 'AudioController')->middleware('hasBrand');
        Route::resource('advertisement', 'AdvertisementController')->middleware('hasBrand');
        // Route::resource('client', 'ClientController');
        // Route::resource('finance', 'FinanceController');
        Route::resource('authority', 'AccessListController');
        // Route::resource('lead-generation', 'LeadGenerationController');

        Route::group(['prefix' => 'profile-management'], function () {
            Route::get('/', 'PageController@profileManagement');
            Route::post('change-password', 'UserController@changePassword');
        });
    });
});
